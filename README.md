Web Slinger
==========

An application to manage a collection of electronic comic book cbr/cbz files.

Author: Brett Vann <brett.vann@vannluminous.com>
URL: http://github.com/roninbv/webslinger.git

This file is part of Web Slinger.

Web Slinger was inspired by Sick Beard, but doesn't contain any of it's code. 
I did however, borrow some code from the guy who makes omniverse. 
I was unable to contact him and the software was unlicensed, so I hope he doesn't mind. Thanks!


The idea here is to have one single point at which to both manage your digital comic collection and to
access it from a tablet for downloading to read.

There are other projects out there that do similar things, but not all of the things Web Slinger does.

Web Slinger is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Web Slinger is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Web Slinger.  If not, see <http://www.gnu.org/licenses/>.


Dependencies
================================
The following are python modules that web slinger depends on, but are not yet bundled with web slinger:

1. CherryPy
2. Mako
3. Rarfile
4. Pymongo. (this has been deprecated, but is not yet removed from the source.)
