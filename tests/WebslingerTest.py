# Author: Brett Vann <brett.vann@vannluminous.com>
# URL: http://github.com/roninbv/webslinger/
#
# This file is part of Web Slinger.
#
# Web Slinger is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Web Slinger is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Web Slinger.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from webslinger.util.parsers import DiamondParser


class DiamondParserTests(unittest.TestCase):
	
	def test_parse_diamond(self):
		parser = DiamondParser()
		parser.set_file_location("testdata\diamond.txt")
		dia_list = parser.get_diamond_list();
		print dia_list
		self.assertIsNotNone(dia_list, "The list returned cannot be null.")
		self.assertTrue(len(dia_list) == 7, "The list contains the wrong number of entries")
		
#	def test_add_available_comics(self):
#		self.assertTrue(False, "This test is not implemented.")
		
	def test_diamond_parser_only_gets_comics(self):
		parser = DiamondParser()
		parser.set_file_location("testdata\diamond.txt")
		dia_list = parser.get_diamond_list();
		disallowed = ["MAGAZINES", "BOOKS", "MERCHANDISE"]
		for cat in disallowed:
			self.assertTrue( cat not in dia_list, "There was an invalid category in the list: %s" % cat)
		
	def test_no_trade_paperbacks(self):
		parser = DiamondParser()
		parser.set_file_location("testdata\diamond.txt")
		dia_list = parser.get_diamond_list();
		reg = ''' TP | HC | GN | POSTER | MAGAZINE | \dND PTG | \dRD PTG | \dTH PTG | COMBO PACK | B CVR'''
		for cat in dia_list:
			for entry in dia_list.get(cat):
				self.assertNotRegexpMatches(entry[1], reg, "List contains an invalid entry..")
				
	def test_get_comic_object(self):
		expected = ['MAY120596 ', 'ARTIFACTS #20 [DIG] ', '$3.99']
		self.assertTrue(True);
		
if __name__ == "__main__":
	unittest.main()