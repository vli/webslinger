import unittest, json, os.path

from unittest import TestCase
from webslinger.Services.collection_service import CollectionService
from webslinger.Model.db import SQLConnector
import urllib2


class CollectionServiceTests(TestCase):
    
    
    def test_get_series_info(self):
        url = 'http://api.comicvine.com/volume/%d/?api_key=21bd63c504a1023da5665835be7c17dacd02cd90&format=json' % 42806
        json_string =  urllib2.urlopen(url).read();
        json_obj = json.loads(json_string);
        for issue in json_obj["results"]["issues"]:
            url = 'http://api.comicvine.com/issue/%d/?api_key=21bd63c504a1023da5665835be7c17dacd02cd90&format=json' %  issue["id"]
            issue_string =  urllib2.urlopen(url).read();
            issue_obj = json.loads(issue_string)["results"];
            print issue_obj["image"]["super_url"]
            #save to database.
        self.assertEqual(json_obj["status_code"], 1, "The service returned an error.")
        