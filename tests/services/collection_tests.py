# Author: Brett Vann <brett.vann@vannluminous.com>
# URL: http://github.com/roninbv/webslinger/
#
# This file is part of Web Slinger.
#
# Web Slinger is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Web Slinger is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Web Slinger.  If not, see <http://www.gnu.org/licenses/>.

import unittest, json, os.path

from unittest import TestCase
from webslinger.Services.collection_service import CollectionService
from webslinger.Model.db import SQLConnector


class CollectionServiceTests(TestCase):
    
    def setUp(self):
        self.service = CollectionService()
        self.db = SQLConnector()
        self.db.initialize_database()
        TestCase.setUp(self)
    
    def test_parse_collection(self):
        file_path = r"../../tests/testdata/Test Collection"
        collection = self.service.parse_collection(file_path)
        for issue in collection:
            if not self.db.collection_title_exists(issue.title, issue.issue_number):
                print issue
                full, thumb = self.service.get_images(issue.url)
                issue.image = full
                issue.thumb = thumb
                self.db.add_collection_title(issue.__dict__)
            
        self.assertGreater(len(collection), 0, "The collection size should be greater than 0")
        
        
    def test_get_thumbnail_cbz(self):
        file_name = "../../tests/testdata/Blue Beetle 001 (2011).cbz"
        
        fullsize,thumb = self.service.get_thumbnail_cbz(file_name)
        self.assertIsNotNone(fullsize, "The fullsize image is empty.")
        self.assertIsNotNone(thumb, "The thumbnail image is empty.")
        
    def test_get_thumbnail_cbr(self):
        file_name = "../../tests/testdata/Blue Beetle 002.cbr"
        
        fullsize,thumb = self.service.get_thumbnail_cbr(file_name)
        self.assertIsNotNone(fullsize, "The fullsize image is empty.")
        self.assertIsNotNone(thumb, "The thumbnail image is empty.")
        
        
#    def test_get_mime_type(self):
#        pass