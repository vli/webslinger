#!/usr/bin/python2

# Author: Brett Vann <brett.vann@vannluminous.com>
# URL: http://github.com/roninbv/webslinger/
#
# This file is part of Web Slinger.
#
# Web Slinger is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Web Slinger is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Web Slinger.  If not, see <http://www.gnu.org/licenses/>.
import getopt
import time
import signal
import sys
import threading
import os
import webslinger

if sys.version_info < (2, 5):
    print("Sorry, Web Slinger requires Python 2.5 or greater.")
    sys.exit(1)

signal.signal(signal.SIGINT, webslinger.signal_handler)
signal.signal(signal.SIGTERM, webslinger.signal_handler)


def help_message():
    """
    print help message for commandline options
    """
    help_msg = "\n"
    help_msg += "Usage: " + webslinger.FULLNAME + " <option> <another option>\n"
    help_msg += "\n"
    help_msg += "Options:\n"
    help_msg += "\n"
    help_msg += "    -h          --help              Prints this message\n"
    help_msg += "    -d          --daemon            Run as double forked daemon (includes options --quiet --nolaunch)\n"


    return help_msg


def main():
    '''
    Comic Books forever!
    '''

    webslinger.FULLNAME = os.path.normpath(os.path.abspath(__file__))
    webslinger.DAEMON = False
    webslinger.CREATEPID = False
    webslinger.PIDFILE = "/var/run/webslinger/webslinger.pid"
    
    threading.currentThread().name = "MAIN"

    try:
        opts, args = getopt.getopt(sys.argv[1:], "hd::", ['help', 'daemon'])  # @UnusedVariable
    except getopt.GetoptError:
        sys.exit(help_message())

    for o, a in opts:

        # Prints help message
        if o in ('-h', '--help'):
            sys.exit(help_message())

        # Run as a double forked daemon
        if o in ('-d', '--daemon'):
            webslinger.DAEMON = True
            webslinger.CREATEPID = True
            
    if webslinger.DAEMON:
        daemonize()
    
    webslinger.initialize(console_log=False)
    webslinger.start()
    
    
    # Stay alive while my threads do the work
    while (True):

        if webslinger.ws_command:
            webslinger.ws_command()
            webslinger.ws_command = None

        time.sleep(1)
    
    return

def daemonize():
    """
    Fork off as a daemon
    """

    # pylint: disable=E1101
    # Make a non-session-leader child process
    try:
        pid = os.fork()  # @UndefinedVariable - only available in UNIX
        if pid != 0:
            os._exit(0)
    except OSError, e:
        sys.stderr.write("fork #1 failed: %d (%s)\n" % (e.errno, e.strerror))
        sys.exit(1)

    os.setsid()  # @UndefinedVariable - only available in UNIX

    # Make sure I can read my own files and shut out others
    prev = os.umask(0)
    os.umask(prev and int('077', 8))

    # Make the child a session-leader by detaching from the terminal
    try:
        pid = os.fork()  # @UndefinedVariable - only available in UNIX
        if pid != 0:
            os._exit(0)
    except OSError, e:
        sys.stderr.write("fork #2 failed: %d (%s)\n" % (e.errno, e.strerror))
        sys.exit(1)

    # Write pid
    if webslinger.CREATEPID:
        pid = str(os.getpid())
        print(u"Writing PID: " + pid + " to " + str(webslinger.PIDFILE))
        try:
            file(webslinger.PIDFILE, 'w').write("%s\n" % pid)
        except IOError, e:
            sys.stderr.write(u"Unable to write PID file: " + webslinger.PIDFILE + " Error: " + str(e.strerror) + " [" + str(e.errno) + "] \n")
            sys.exit(1)

    # Redirect all output
#         sys.stdout.flush()
#         sys.stderr.flush()
#     
#         devnull = getattr(os, 'devnull', '/dev/null')
#         stdin = file(devnull, 'r')
#         stdout = file(devnull, 'a+')
#         stderr = file(devnull, 'a+')
#         os.dup2(stdin.fileno(), sys.stdin.fileno())
#         os.dup2(stdout.fileno(), sys.stdout.fileno())
#         os.dup2(stderr.fileno(), sys.stderr.fileno())


if __name__ == "__main__":
    
    if webslinger.CONFIG_FILE is None:
            webslinger.CONFIG_FILE = "webslinger.ini"
    
    main()
