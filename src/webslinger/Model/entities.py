# Author: Brett Vann <brett.vann@vannluminous.com>
# URL: http://github.com/roninbv/webslinger/
#
# This file is part of Web Slinger.
#
# Web Slinger is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Web Slinger is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Web Slinger.  If not, see <http://www.gnu.org/licenses/>.



class IssueType(object):
    NORMAL = 1
    ANNUAL = 2
    KING_SIZE = 4
    GIANT_SIZE = 8
    SPECIAL = 16
    
class Category(object):
    
    def __init__(self, text, cat_id = None):
        self._id = id
        self.text = text
    
class Publisher(object):
    
    def __init__(self, text, pub_id = None):
        self._id = pub_id
        self.text = text
    
    
class ComicSeries(object):
        
    def __init__(self, series_id, title, series_type, category_id = None, comic_vine_id = None, publisher = None, image = None, icon = None):
        self.id = series_id
        self.title = title
        self.publisher = publisher
        self.image_url = image
        self.icon_url = icon
        self.comic_vine_id = comic_vine_id
        self.series_type = series_type

    def __unicode__(self):
        return unicode(self.title)

     
class ComicIssue():
    
    def __init__(self, series_id,  title, issue_number, url, volume=0, release_year = 0,
                 thumb = None, image = None, read = False, is_digital = False, issue_type = IssueType.NORMAL, comic_vine_id = None, _id = None):
        self.thumb = thumb
        self.id = _id
        self.image = image
        self.url = url
        self.volume = volume
        self.publish_year = release_year
        self.series_id = series_id
        self.title = title
        self.issue_type = issue_type
        self.is_digital = is_digital
        self.issue_number = issue_number
        self.is_read = read
        self.comic_vine_id = comic_vine_id
        
    def __str__(self):
        return "%s v%d #%.1f (%d) @%s" % (self.title, self.volume, self.issue_number, self.release_year, self.url)
        

        
# Settings classes.
        
class GeneralSettings():
    
    def __init__(self,comic_vine_api = None, path = None, rescan_freq = 1, rescan_denom =1):
        self.comic_vine_api = comic_vine_api
        self.path = path
        self.rescan_freq = rescan_freq
        self.rescan_denom = rescan_denom
        self.cwd = "";
        
class DatabaseSettings():
    
    def __init__(self):
        self.store_type = None
        self.db_name = None
        self.db_host = None
        self.db_port = None
        self.db_user = None
        self.db_password = None
        
class NNTPSettings():
    pass