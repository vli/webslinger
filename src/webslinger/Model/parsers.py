# Author: Brett Vann <brett.vann@vannluminous.com>
# URL: http://github.com/roninbv/webslinger/
#
# This file is part of Web Slinger.
#
# Web Slinger is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Web Slinger is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Web Slinger.  If not, see <http://www.gnu.org/licenses/>.

import regexes as regexes
import urllib2
import StringIO
import re
import rarfile
import zipfile
from rarfile import RarFile
from zipfile import ZipFile
from datetime import datetime
from PIL import Image
from webslinger.Model.entities import IssueType


class DiamondParser(object):
    '''
    The DiamondParser is used to parse text files supplied as new 
    release lists from Diamond comic book distributor. 
        This parser specifically looks only for comicbooks and avoids
        magazines, toys, etc.
    '''

    def __init__(self):
        '''
        Create an instance of the Diamond file parser.
        '''
        self.file_date = None
        self.new_releases = None
        pass


    def set_file_location(self, filename_or_url):
        '''
        Opens either a url or a local file to parse.
        '''
        if 'http' in filename_or_url:
            self.new_releases = urllib2.urlopen(filename_or_url)
        else:
            self.new_releases = open(filename_or_url)

    @staticmethod
    def create_comic_list(line):
        '''
        Cleanses unnecessary characters from the line, removes blank lines, and builds
        lists based on line contents.
        '''
        if type(line) != str:
            line = line.decode()
        if line != 'r\n' or '\n':
            line = line.replace('\r\n', '')
            line = line.replace('\n', '')
            if '\t' in line:
                line = line.split('\t')
                return line
            else:
                return line

    def cleanse_diamond_list(self, diamond_list):

        while None in diamond_list:
            diamond_list.remove(None)

        while '' in diamond_list:
            diamond_list.remove('')

            # GET THE DATE FROM diamond_list[]
        match = re.search(r"\d+/\d+/\d+", diamond_list[0])
        if match is not None:
            self.file_date = datetime.strptime(match.group(0).strip(), '%m/%d/%Y')
        else:
            match = re.search(r"\d+/\d+/\d+", diamond_list[1])
            if match is not None:
                self.file_date = datetime.strptime(match.group(0).strip(), '%m/%d/%Y')
            else:
                self.file_date = None

        return diamond_list[4:]


    def get_diamond_list(self):
        try:
            reg = r" TP | HC | GN | POSTER | MAGAZINE | \dND PTG | \dRD PTG | \dTH PTG | COMBO PACK | B CVR"
            ordinal_regex = re.compile(reg)
            diamond_list = self.cleanse_diamond_list(list(map(DiamondParser.create_comic_list, self.new_releases)))
            current_cat = ''
            comic_dict = {}

            disallowed = ["BOOKS", 'MERCHANDISE', 'COLLECTIBLES', 'TOYS', "MAGAZINES", "SUPPLIES", "PREMIER PUBLISHERS",
                          "PREVIEWS PUBLICATIONS"];

            for element in diamond_list:
                if type(element) == str:
                    current_cat = element
                    continue

                else:
                    if current_cat in comic_dict.keys():

                        if ordinal_regex.search(element[1]) is None:
                            element.append(self.file_date)
                            comic_dict[current_cat].append(element)

                    elif ordinal_regex.search(element[1]) is None:

                        allow = True
                        for word in disallowed:
                            if current_cat.find(word) > -1:
                                allow = False

                        if allow:
                            comic_dict[current_cat] = list()
                            element.append(self.file_date)
                            comic_dict[current_cat].append(element)

            return comic_dict
        except Exception as e:
            print(e)


    def get_available_comic_object(self, comic_tuple):
        raise NotImplementedError()


class NewsGroupHeaderParser(object):
    def __init__(self, file_name=True):
        self.file_name = file_name
        self.compiled_regexes = []
        self._compile_regexes()
        self.current_match = None

    def _compile_regexes(self):
        for (cur_pattern_name, cur_pattern) in regexes.ish_regexes:
            try:
                cur_regex = re.compile(cur_pattern, re.VERBOSE | re.IGNORECASE)
            except re.error as err:
                print (err)
            else:
                self.compiled_regexes.append((cur_pattern_name, cur_regex))

    def parse(self, name):
        if not name:
            return None

        for (cur_reg_name, cur_regex) in self.compiled_regexes:
            self.current_match = cur_regex.match(name)

            if not self.current_match:
                continue

            parse_result = self.create_result(cur_reg_name, name)
            return cur_reg_name, parse_result

        return None


    def display_match(self):
        if self.current_match is None:
            return None
        return '<Match: %r, groups=%r>' % (self.current_match.group(), self.current_match.groups())

    def create_result(self, regex_used, filename):


        if regex_used == 'zero_day':
            return ParseResult(filename, self.current_match.group("comic_title"),
                               int(self.current_match.group("issue_num")),
                               year=int(self.current_match.group("year")),
                               group=self.current_match.group('group')).__dict__
        if regex_used == 'complete':
            return ParseResult(filename, self.current_match.group("comic_title"),
                               int(self.current_match.group("issue_num")),
                               volume=self.current_match.group("volume"),
                               month=self.current_match.group("month"),
                               year=int(self.current_match.group("year")),
                               group=self.current_match.group('group')).__dict__
        if regex_used == 'short':
            return ParseResult(filename, self.current_match.group("comic_title"),
                               int(self.current_match.group("issue_num"))).__dict__

        if regex_used == 'quoted':
            return ParseResult(filename, self.current_match.group("comic_title"),
                               int(self.current_match.group("issue_num"))).__dict__


class ParseResult(object):
    def __init__(self,
                 file_name,
                 comic_title,
                 issue_num,
                 volume=1,
                 year=None,
                 month=None,
                 group=None):
        self.file_name = file_name
        self.comic_title = comic_title
        self.volume = volume
        self.issue_num = issue_num
        self.year = year
        self.month = month
        self.group = group

    def __str__(self):
        if self.comic_title is not None:
            to_return = self.comic_title + ' - '
        else:
            to_return = ''
        if self.issue_num is not None:
            to_return += '#' + str(self.issue_num)
        if self.year is not None:
            to_return += ' (' + str(self.year) + ') '
        if self.month is not None:
            to_return += str(self.month)
        if self.group is not None:
            to_return += ' (' + str(self.group) + ")"

        return to_return.encode('utf-8')

    def __eq__(self, other):
        if other is None:
            return False
        if self.file_name != other.file_name:
            return False
        if self.comic_title != other.comic_title:
            return False
        if self.issue_num != other.issue_num:
            return False
        if self.year != other.year:
            return False
        if self.month != other.month:
            return False
        if self.group != other.group:
            return False
        return True


class CollectionParser(object):
    PORTRAIT = 100
    LANDSCAPE = 225

    def __init__(self):
        pass

    @staticmethod
    def find_series_type(path):
        match = re.search(r'([Mm]ini\-[Ss]eries)', path)
        if match != None:
            return "Mini-Series"
        else:
            match = re.search(r'([Oo]ne\ [Ss]hot)', path)
            if match is not None:
                return "One-Shot"

            return "Ongoing"

    @staticmethod
    def find_issue_number(issue_string):
        issue_num = 0
        match = re.search(r"(\#[\d\.]+)", issue_string)
        if match is not None:
            issue_number = match.group(0)[1:]
            if issue_number[-1] == '.':
                issue_number = issue_number[:-1]
            issue_num = float(issue_number)
        else:
            match = re.search(r"(\ [\d\.]+)", issue_string)
            if match is not None:
                issue_number = match.group(0)[1:]
                if issue_number[-1] == '.':
                    issue_number = issue_number[:-1]
                issue_num = float(issue_number)
        return issue_num

    @staticmethod
    def determine_issue_mask(issue_string):
        mask = IssueType.NORMAL
        match = re.search(r'\ [aA]nnual', issue_string)
        if match is not None:
            mask = mask | IssueType.ANNUAL

        match = re.search(r'[Gg]iant[- ][Ss]ize', issue_string)
        if match is not None:
            mask = mask | IssueType.GIANT_SIZE

        match = re.search(r'[Kk]ing[- ][Ss]ize', issue_string)
        if match is not None:
            mask = mask | IssueType.KING_SIZE

        match = re.search(r'[Ss]pecial', issue_string)
        if match is not None:
            mask = mask | IssueType.SPECIAL

        return mask

    @staticmethod
    def find_issue_title(issue_string):
        match = re.search(r'.*\(', issue_string)
        if match is not None:
            return match.group(0)[:-1].strip()
        else:
            return issue_string

    @staticmethod
    def find_issue_volume(containing_folder):
        match = re.search(r'\(\d\d\d\d\)+', containing_folder)
        if match is not None:
            return int(match.group(0)[1:-1])
        else:
            return 0

    @staticmethod
    def find_publisher(collection_path, issue_string):
        root_lvl = len(collection_path.split('/'))
        return issue_string.split('/')[root_lvl]

    @staticmethod
    def find_publish_year(issue_string):
        match = re.search(r'\(\d+\)', issue_string)
        if match is not None:
            return int(match.group(0).replace('(', '').replace(')', ''))
        else:
            return 0

    @staticmethod
    def get_image_thumbnail(full_image):
        temp = Image.open(StringIO.StringIO(full_image))
        if temp.size[0] < temp.size[1]:
            width_percent = CollectionParser.PORTRAIT / float(temp.size[0])
            thumb_height = int(float(temp.size[1]) * float(width_percent))
            thumb_width = CollectionParser.PORTRAIT
        else:
            width_percent = CollectionParser.LANDSCAPE / float(temp.size[0])
            thumb_height = int(float(temp.size[1]) * float(width_percent))
            thumb_width = CollectionParser.LANDSCAPE

        temp_thumb = temp.resize((thumb_width, thumb_height), Image.ANTIALIAS)
        thumb_image = StringIO.StringIO()
        temp_thumb.save(thumb_image, 'jpeg')
        thumb = thumb_image.getvalue()
        thumb_image.close()
        return thumb

    def get_thumbnail_cbr(self, filename):
        rar_archive = None

        if rarfile.is_rarfile(filename) is not True:
            # sometimes, cbrs are actually zip files.
            return self.get_thumbnail_cbz(filename)

        try:
            rar_archive = RarFile(filename)
            names = rar_archive.namelist()
            names.sort()
            cover_name = CollectionParser._find_cover(names)
            full_image = rar_archive.read(cover_name)
            thumb = self.get_image_thumbnail(full_image)
            return full_image, thumb
        except Exception as e:
            print e
            return None, None
        finally:
            if rar_archive is not None:
                rar_archive.close()

    def get_thumbnail_cbz(self, filename):
        zip_archive = None

        if zipfile.is_zipfile(filename) is not True:
            return None, None

        try:
            # open the cbz
            zip_archive = ZipFile(filename)
            names = zip_archive.namelist()
            names.sort()
            cover_name = CollectionParser._find_cover(names)
            full_image = zip_archive.read(cover_name)
            thumb = self.get_image_thumbnail(full_image)
            return full_image, thumb
        except:
            return None, None
        finally:
            if zip_archive is not None:
                zip_archive.close()

    @staticmethod
    def _find_cover(names):
        for name in names:
            if ("cover" in name or "COVER" in name or "0fc" in name) and (".jpg" in name or '.JPG' in name):
                return name

        for name in names:
            if (".jpg" in name or ".JPG" in name ) and (
                                'join' not in name and 'JOIN' not in name and 'tag' not in name):
                # this will be the first image
                return name
        return None


class ArticleParser():
    def __init__(self, bad_file):
        with open(bad_file, 'r') as f:
            lines = f.readlines()
            self.re_bad = []
            for line in lines:
                self.re_bad.append(re.compile(line, re.I))

    def subject_to_totals(self, subject):
        re_parts = re.compile(r"\((?P<part>\d+)/(?P<total_parts>\d+)\)")
        matches = re_parts.search(subject)
        if matches:
            return (matches.group("part"), matches.group("total_parts"))
        else:
            return (None, None)

    def subject_to_yenc(self, subject):
        re_yenc = re.compile(r"yEnc")
        matches = re_yenc.search(subject)
        if matches:
            return True
        else:
            return False

    def subject_to_size(self, subject):
        re_size = re.compile(r"(?P<size>\d+KB)")
        matches = re_size.search(subject)
        if matches:
            return matches.group("size")
        else:
            return None

    def subject_to_filename(self, subject):
        re_filename = re.compile(
            r"\"??(?P<filename>[^\"]*?(\.part\d{1,2}\.rar|\.vol\d{1,2}\+\d{1,2}|\.cbr|\.cbz|\.pdf|\.rar|\.par2|\.par|\.zip|\.sfv|\.nfo|\.nzb)+)\"??",
            re.I)
        re_x_of_y = re.compile(r"\d{1,3} of \d{1,3} - ")
        matches = re_filename.search(subject)
        if matches:
            # remove any text such as "1 of 8"
            return re_x_of_y.sub("", matches.group("filename")).strip()
        else:
            return None

    def subject_to_similar(self, subject):
        re_similar = re.compile(r"\)(\d+)/\d+\(")
        re.sub
        return re_similar.sub(")/1(", subject[::-1], 1)[::-1]

    def bad_filter(self, text):
        # for r in self.re_bad:
        # if r.search(text):
        # return True
        return False


if __name__ == "__main__":
    # a = ArticleParser('../../bad.txt')
    # assert (a.subject_to_filename(
    #     '(1/17) "Heavy Metal Vol.9.rar" - 713.49 MB - Heavy Metal (Complete Comic) Vol.9 yEnc (1/131)') ==
    #         "Heavy Metal Vol.9.rar")
    # assert (a.subject_to_filename(
    #     '(1/17) "Heavy Metal Vol.9.part1.rar" - 713.49 MB - Heavy Metal (Complete Comic) Vol.9 yEnc (1/261)') ==
    #         "Heavy Metal Vol.9.part1.rar")
    #
    # assert (a.subject_to_filename('(Kroost 6) [4/7] - "Kroost 6.cbr.vol01+2.PAR2" yEnc (1/)') ==
    #         "Kroost 6.cbr.vol01+2.PAR2")
    # assert (a.subject_to_filename(
    #     "(I'M BA-A-A-A-ACK!!!!) Kull The Destroyer #25 ||1978 || FBScan || [4/5] - \"Kull The Destroyer 025 (1978) (FBScan).CBR.vol1+2.PAR2\"") ==
    #         "Kull The Destroyer 025 (1978) (FBScan).CBR.vol1+2.PAR2")

    d = DiamondParser()
    d.set_file_location("http://www.previewsworld.com/shipping/newreleases.txt")
    diamond_list = d.get_diamond_list()

    for ele in diamond_list:
        print "Parsing %s from diamond" % ele
        for elm in diamond_list[ele]:
            #if self.connection.check_diamond_title_exists == False:
            ish = {'code': elm[0], 'title': elm[1], 'price': elm[2],
                   'publisher': ele, "publish_date": elm[3].strftime('%m-%d-%Y') }
            print ish;