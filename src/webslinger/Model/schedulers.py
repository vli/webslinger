# Author: Brett Vann <brett.vann@vannluminous.com>
# URL: http://github.com/roninbv/webslinger/
#
# This file is part of Web Slinger.
#
# Web Slinger is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Web Slinger is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Web Slinger.  If not, see <http://www.gnu.org/licenses/>.

from datetime import datetime, timedelta
import threading
import time
import webslinger


class Scheduler:
    
    def __init__(self, action, cycle_time, thread_name = "schedulerThread", start_immediately = False, quiet = True ):
        
        if start_immediately:
            self.last_run = datetime.fromordinal(1)
        else:
            self.last_run = datetime.now()
        
        self.thread = None
        self.action = action
        self.cycle_time = timedelta(minutes = cycle_time)
        self.thread_name = thread_name
        self.start_immediately = start_immediately
        self.quiet = quiet
        self.abort = False;
        self.init_thread()
        
    def init_thread(self):
        if self.thread is None or not self.thread.isAlive():
            self.thread = threading.Thread(None, self.run_action, self.thread_name) 
        
    def force_run(self):
        if not self.action.is_awake:
            self.last_run = datetime.fromordinal(1)
            return True
        return False
    
    def time_remaining(self):
        return self.cycle_time - (datetime.now() -self.last_run)
    
    
    def run_action(self):
        
        while True:
            
            now = datetime.now()
            
            if now - self.last_run > self.cycle_time:
                self.last_run = now
                try:
                    if not self.quiet:
                        webslinger.LOGGER.log_info("Starting Service Thread: " + self.thread_name)
                    self.action.run()
                except Exception, e:
                    webslinger.LOGGER.log_exception(e)
            
            if self.abort:
                self.abort = False
                self.thread = None
                return
            
            time.sleep(1)
            
            
    