# Author: Brett Vann <brett.vann@vannluminous.com>
# URL: http://github.com/roninbv/webslinger/
#
# This file is part of Web Slinger.
#
# Web Slinger is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Web Slinger is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Web Slinger.  If not, see <http://www.gnu.org/licenses/>.
import Queue
import sqlite3
import MySQLdb
import threading
import webslinger
from datetime import datetime
from webslinger.Model.entities import IssueType
from webslinger.Services.comic_vine_collector import ComicVineInsertService, ComicVineUpdateService

class DataAccessFactory():
    SQLITE =1
    MYSQL  =2

    def __init__(self):
        pass
    
    def get_db_connector(self, db_type, host_or_file, db_name = None, db_user = None, db_passwd = None):
        if db_type == self.SQLITE:
            return SqliteConnector(host_or_file)
        elif db_type == self.MYSQL:
            connection_params = {'host': host_or_file, 'user': db_user, 'passwd': db_passwd, 'dbname': db_name, 'dbstore': self.MYSQL}
            return MySQLConnector(connection_params)

class MultiThreadSQLite(threading.Thread):
    def __init__(self, db, autostart=False):
        super(MultiThreadSQLite, self).__init__(name="Database Thread")
        self.db=db
        self.CANCEL = False
        self.query_queue=Queue.Queue()
        if autostart:
            self.start()
            
    def run(self):
        connection = sqlite3.connect(self.db) 
        connection.text_factory = str
        connection.create_function("alphanumeric", 1, self.sql_alphanumeric)
        cursor = connection.cursor()
        while not self.CANCEL:
            try:
                req, arg, res = self.query_queue.get(timeout=10)
            except Queue.Empty:
                continue
                
            if req=='--close--': break
            if req=='--commit--':
                connection.commit()
                continue
            
            cursor.execute(req, arg)
            if res:
                for rec in cursor:
                    res.put(rec)
                res.put('--no more--')
        connection.close()
    
    def cancel(self):
        webslinger.LOGGER.log_info("%s will finish canceling after processing approximately %d items." % (self.name, self.query_queue.qsize()))
        self.CANCEL = True
    
    def execute(self, req, arg=None, res=None):
        self.query_queue.put((req, arg or tuple(), res))
    
    def select(self, req, arg=None):
        res=Queue.Queue()
        self.execute(req, arg, res)
        while True:
            rec=res.get()
            if rec=='--no more--': break
            yield rec
    
    def close(self):
        self.execute('--close--')
    
    def commit(self):
        self.execute('--commit--')
        
    def sql_alphanumeric(self,s):
        for index in range(len(s)):
            if s[index].isalnum():
                return s[index:]
            
class MultiThreadMySQL(threading.Thread):
    def __init__(self, db, autostart=False):
        super(MultiThreadMySQL, self).__init__(name="Database Thread")
        self.db=db
        self.CANCEL = False
        self.query_queue=Queue.Queue()
        if autostart:
            self.start()
            
    def run(self):
        connection = MySQLdb.connect(self.db["host"], self.db['user'], self.db['passwd'], self.db['dbname'])
        cursor = connection.cursor()
        while not self.CANCEL:
            try:
                req, arg, res = self.query_queue.get(timeout=10)
            except Queue.Empty:
                continue
                
            if req=='--close--': break
            if req=='--commit--':
                connection.commit()
                continue
            
            try:
                #print req
                #print arg
                cursor.execute(req, arg)
            except:
                raise
            
            
            if res:
                for rec in cursor:
                    res.put(rec)
                res.put('--no more--')
        connection.close()
    
    def cancel(self):
        webslinger.LOGGER.log_info("%s will finish canceling after processing approximately %d items." % (self.name, self.query_queue.qsize()))
        self.CANCEL = True
    
    def execute(self, req, arg=None, res=None):
        self.query_queue.put((req, arg or tuple(), res))
    
    def select(self, req, arg=None):
        res=Queue.Queue()
        self.execute(req, arg, res)
        while True:
            rec=res.get()
            if rec=='--no more--': break
            yield rec
    
    def close(self):
        self.execute('--close--')
    
    def commit(self):
        self.execute('--commit--')
        
    def sql_alphanumeric(self,s):
        for index in range(len(s)):
            if s[index].isalnum():
                return s[index:]     


class SqLiteDataAccess():
    def __init__(self, filename):
        self.connection = MultiThreadSQLite(filename, True)
        
    def execute_select(self, query, params = None):
        return self.connection.select(query, params)
    
    def execute_update(self, query, params):
        self.connection.execute(query, params)
        self.connection.commit()
        
    def execute_insert(self, query, params):
        self.connection.execute(query, params)
        self.connection.commit()
        sql = ''' SELECT last_insert_rowid()'''
        return self.connection.select(sql).next()
        
    
    def execute_query(self, query, params = None):
        res = None
        self.connection.execute(query, params, res)
        return res
    
    def execute_scalar(self, query, params = None):
        return self.connection.select(query,params).next()
    
    def commit(self):
        self.connection.commit()
        
        
class MysqlDataAccess():    
    
    def __init__(self, db_connect_info):
        self.connection = MultiThreadMySQL(db_connect_info, True)
        
    def execute_select(self, query, params = None):
        return self.connection.select(query, params)
    
    def execute_update(self, query, params):
        self.connection.execute(query, params)
        self.connection.commit()
        
    def execute_insert(self, query, params):
        self.connection.execute(query, params)
        self.connection.commit()
        sql = 'SELECT LAST_INSERT_ID()'
        return self.connection.select(sql).next()
        
    
    def execute_query(self, query, params = None):
        res = None
        self.connection.execute(query, params, res)
        return res
    
    def execute_scalar(self, query, params = None):
        return self.connection.select(query,params).next()
    
    def commit(self):
        self.connection.commit()
        
class MySQLConnector:
    
    def __init__(self, connection):        
        self.connection = MysqlDataAccess(connection)
    
    #--- Database init functions
        
    def clear_database(self):
        self.connection.execute_query("DELETE FROM collection_title")
        self.connection.execute_query("DELETE FROM collection_issue")
        self.connection.execute_query("DELETE FROM pull_list")
        self.connection.execute_query("DELETE FROM diamond_title")
        
    def initialize_database(self, truncate_tables):
        
        self.connection.execute_query('''CREATE DATABASE IF NOT EXISTS webslinger_db;''')
        
        self.connection.execute_query(''' 
            CREATE TABLE IF NOT EXISTS collection_title (
                id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
                title VARCHAR(512) NOT NULL,
                volume INT,
                publisher VARCHAR(512),
                start_year INT,
                image_url TEXT,
                icon_url TEXT,
                series_type VARCHAR(25) NOT NULL,
                comic_vine_id INT,
                in_pull_list BOOLEAN NOT NULL DEFAULT FALSE,
                INDEX collection_asc (title ASC)
            );''');
            
        self.connection.execute_query('''     
            CREATE TABLE IF NOT EXISTS collection_issue (
                id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
                series_id INT NOT NULL,                
                title VARCHAR(512),
                issue_number VARCHAR(11) NOT NULL,
                url TEXT,
                issue_type INT NOT NULL,
                is_digital BOOLEAN NOT NULL,
                comic_vine_id INT,                
                publish_date DATETIME,
                image_url TEXT,
                icon_url TEXT,
                image LONGBLOB,
                thumb LONGBLOB,
                status INT NOT NULL,
                is_read BOOLEAN,
                INDEX status_asc (status ASC)
            );''');
            
            
        self.connection.execute_query('''
                CREATE TABLE IF NOT EXISTS diamond_title (
                id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
                title VARCHAR(512) NOT NULL,
                price REAL NOT NULL,
                publisher VARCHAR(100),
                pub_date VARCHAR(100),
                code VARCHAR(10) NOT NULL
            );''')
        
        
        
        self.connection.execute_query('''
            CREATE TABLE IF NOT EXISTS newsgroup(
                id INT PRIMARY KEY AUTO_INCREMENT,
                name VARCHAR(512) NOT NULL,
                size INT NOT NULL)
            ''')
        
        self.connection.execute_query('''
                                        CREATE TABLE IF NOT EXISTS group_setting(
                                         id INT PRIMARY KEY AUTO_INCREMENT,
                                         name TEXT,
                                         last_article INTEGER NOT NULL
                                         )''')
        
        self.connection.execute_query('''
            CREATE TABLE IF NOT EXISTS article(
                id INTEGER PRIMARY KEY AUTO_INCREMENT,
                subject TEXT NOT NULL, 
                parts TEXT, 
                total_parts INT, 
                complete BIT, 
                filename TEXT,
                groups TEXT, 
                poster TEXT, 
                date_posted INT,
                size INT, 
                yenc BIT
                );''')
        
        
        if truncate_tables:
            self.clear_database()
            
    #--- Diamond Parser methods.
            
    def clear_diamond_table(self):
        self.connection.execute_query("DROP TABLE diamond_title")
            
    def check_diamond_title_exists(self, title):
        sql = '''
         SELECT COUNT(id) from diamond_title WHERE title = %s
        '''
        parm = [title]
        
        result = self.connection.execute_scalar(sql, parm)
        return result[0] > 0
        
            
    def add_available_title(self, title):
        sql = '''
            INSERT INTO diamond_title
            (title, price, code, pub_date, publisher)
            VALUES
            (%s,%s,%s,%s,%s)
        '''
        param = [title.title, title.price, title.code, title.publish_date, title.publisher]
        self.connection.execute_insert(sql, param);        
    
    #--- Collection Methods.
            
    def get_collection_size(self):
        sql = ''' 
         SELECT count(id) FROM collection_title
        '''
        title_count = self.connection.execute_scalar(sql)[0];
        
        sql = ''' 
         SELECT count(id) FROM collection_issue
         WHERE status = 8
        '''
        issue_count = self.connection.execute_scalar(sql)[0];
        
        return ( title_count, issue_count)

    #--- Publisher methods

    def get_publisher_exists(self, publisher_name):
        sql = "SELECT * FROM collection_publisher where name = %s"
        param = [publisher_name];

        result = self.connection.execute_select(sql, param)

        print result
        return result[0]['name']
        
    #--- Title methods

    def get_collection_titles(self, show_mini = False, show_regular = True, show_one_shot = False ):
        if show_mini is False and show_regular is False and show_one_shot is False:
            return {}

        series_types = []
        if show_regular:
            series_types.append("'Ongoing'")

        if show_mini:
            series_types.append("'Mini-Series'")

        if show_one_shot:
            series_types.append("'One-Shot'")

        sql = '''
        SELECT id, title, volume, comic_vine_id FROM collection_title
        WHERE series_type IN ('''

        sql += ', '.join(series_types)

        sql += ''')
        order by title, volume;
        '''
        result = self.connection.execute_select(sql)

        return result
    
    def collection_title_exists(self, title, volume ):
        query = '''
            SELECT count(id) as num, id FROM collection_title
            WHERE title = %s 
            AND volume = %s;
            '''
        params = [title, volume]
        result = self.connection.execute_scalar(query, params)
        if result[0] > 0:
            return result[1]
        else:
            return None
        
    def add_collection_title(self, title):
        sql = '''
            INSERT INTO collection_title (title, volume, in_pull_list, series_type)
            VALUES
            (%s,%s,%s,%s);
            '''
        params = [title["title"], title["volume"], False, title["series_type"]]
        return self.connection.execute_insert(sql, params)[0] 
    
    #--- Issue Methods
    
    def get_collection_issues(self, series_id, page=1, rowcount = 20, sortcol = 'issue_number', sortord = 'ASC', issue_type = IssueType.NORMAL):
        sql = '''
        SELECT id, title, issue_number, url, is_read, issue_type, icon_url, comic_vine_id FROM collection_issue
        WHERE series_id = %s
        '''
        if issue_type != IssueType.NORMAL:
            sql += '''AND issue_type  & %s '''
        else:
            sql += '''AND issue_type = %s'''
        
        sql += '''
        AND status = 8
        ORDER BY CAST(issue_number as SIGNED) ASC, issue_number ASC
        LIMIT %s OFFSET %s
        '''
        params = [series_id, issue_type, rowcount, (page - 1) * rowcount]
        print params;
        result = self.connection.execute_select(sql, params)
        
        if issue_type != IssueType.NORMAL:
            sql = "SELECT count(id) FROM collection_issue WHERE series_id = %s AND issue_type & %s"
        else:
            sql = "SELECT count(id) FROM collection_issue WHERE series_id = %s AND issue_type = %s"
        
        count = self.connection.execute_scalar(sql, [series_id, issue_type])[0]
        
        if issue_type != IssueType.NORMAL:
            sql = "SELECT count(id) FROM collection_issue WHERE series_id = %s AND issue_type & %s AND status = 8"
        else:
            sql = "SELECT count(id) FROM collection_issue WHERE series_id = %s AND issue_type = %s AND status = 8"
        
        in_collection = self.connection.execute_scalar(sql, [series_id, issue_type])[0]
        
        objects = list()
        index = 1
        for data in result:
            obj = {"id": data[0],"issueNumber": data[2], "url": data[3], "icon_url": data[6],
                    "title": data[1], "isRead": data[4], "index": index, 'issue_type': data[5], 'comic_vine_id': data[7]}
            objects.append(obj)

        total = (count / rowcount)
        if count % rowcount != 0:
            total = total + 1
        
        return {"page": page, "rows": objects, "records": count, "in_collection": in_collection, "total_pages": total}
        
            
    def collection_issue_exists(self, series_id, issue_number, issue_type, issue_url = None ):
        sql = '''
            SELECT count(id) FROM collection_issue
            WHERE series_id = %s 
            AND issue_number = %s
            AND status = 8            
            '''
        if issue_type != IssueType.NORMAL:
            sql += '''AND issue_type  & %s '''
        else:
            sql += "AND issue_type = %s "
            
        params = [series_id,issue_number, issue_type]
            
        if issue_url is not None:
            sql += "AND url = %s"
            params.append(issue_url)
            
        result = self.connection.execute_scalar(sql, params)
        return result[0] > 0
    
    def add_collection_issue(self, issue):
        sql ='''
            INSERT INTO collection_issue
            (series_id, issue_number, url, issue_type, is_digital,  image, thumb, is_read, status)
            VALUES
            (%s, %s, %s, %s, %s, %s, %s, %s, %s);
        '''
        parameters = [issue.series_id, issue.issue_number, issue.url, issue.issue_type, issue.is_digital,
                      issue.image, issue.thumb, issue.is_read, 8]
        self.connection.execute_insert(sql, parameters)
        
    def update_issue_read(self, issue_id, is_read):   
        sql = '''
            UPDATE collection_issue SET
            is_read = %s
            WHERE id = %s;            '''
        
        params = [is_read, issue_id]
        self.connection.execute_update(sql, params);
        
        
    def get_thumb(self, issue_id):
        query = '''
            SELECT thumb FROM collection_issue
            WHERE id = %s
            '''
        result = self.connection.execute_select(query, [issue_id])
        return result.next()
    
    def get_image(self, issue_id = None):
        query = '''
            SELECT image FROM collection_issue
            WHERE id = %s
            '''
        if issue_id is None or issue_id == "null":
            f = open('webslinger/static/images/ImageNotAvailable.jpg', 'rb')
            buff = f.read()
            f.close()
            return buff
        try:
            result = self.connection.execute_select(query, [issue_id])
            return result.next()
        except:
            f = open('webslinger/static/images/ImageNotAvailable.jpg', 'rb')
            buff = f.read()
            f.close()
            return buff
        
        pass
        
    #--- PULL LIST METHODS
    
    def get_is_in_pull(self, _id):
        query = ''' SELECT in_pull_list FROM collection_title WHERE id = %s'''
        
        params = [_id]
        try:
            result = self.connection.execute_select(query, params).next();
            
            return result[0] ==1
        except:
            return None
    
    def add_pull_list_title(self, title):
        sql = '''
            INSERT INTO collection_title 
            (title,  publisher, image_url, comic_vine_id, in_pull_list)
            VALUES
            (%s,%s,%s,%s,1)
        '''
        params =[title["title"], title["publisher"], title["image_url"], title["comic_vine_id"]]
        series_id = self.connection.execute_insert(sql, params)
        collector = ComicVineInsertService(title["comic_vine_id"], series_id[0]);
        collector.start()
        
    def map_series_to_pull_list(self,title ):
        sql ='''
            UPDATE collection_title 
            SET 
            comic_vine_id = %s, 
            publisher = %s,
            image_url = %s,
            in_pull_list = 1
            WHERE id = %s'''
        self.connection.execute_update(sql, [title['cv_id'], title['publisher'], title['image_url'], title['series_id']])
        collector = ComicVineUpdateService(self, title["series_id"], title["cv_id"])
        collector.start()
        
    def insert_comic_vine_issue(self, series_id, issue_obj):
        sql ='''
                    INSERT INTO collection_issue (series_id, title, issue_number,issue_type, is_digital, status, comic_vine_id, publish_date, is_read, image_url, icon_url)
                    VALUES
                    (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
                '''
        parm = [series_id, issue_obj["name"], issue_obj["issue_number"], 1, True, 1,
                issue_obj["id"], issue_obj["cover_date"], False,
                issue_obj["image"]["super_url"],issue_obj["image"]["thumb_url"]]
        self.connection.execute_insert(sql, parm)
        
     
    def update_issue_with_comic_vine_info(self, series_id, issue_obj):
        sql = '''
                        UPDATE collection_issue
                        SET 
                        title = %s,
                        status = 8,
                        comic_vine_id = %s,
                        publish_date = %s,
                        image_url = %s,
                        icon_url = %s
                        WHERE series_id = %s
                        AND issue_number = %s
                        AND issue_type = %s
                    '''
        parm = [issue_obj['name'],issue_obj["id"],issue_obj["cover_date"],issue_obj["image"]["super_url"],
                issue_obj["image"]["thumb_url"], series_id, issue_obj["issue_number"], 1]
        
        self.connection.execute_update(sql, parm)
       
    
    
    def delete_pull_title(self, _id):
        sql = '''UPDATE  collection_title SET in_pull_list = 0  WHERE id = %s'''
        self.connection.execute_insert(sql, [_id])    
    
    def get_current_pull_list(self, page = 1, rows = 10, sortcol = "title", sortord = 1):
        sql = '''
        SELECT * FROM collection_title WHERE in_pull_list = 1 order by publisher, title ;
        '''
        
        result = self.connection.execute_select(sql)
        objects = []
        for row in result:
            temp = {"id": row[0], "title": row[1], "volume": row[2], "publisher": row[3], 
             "image_url": row[5], "comic_vine_id": row[7] }
            objects.append(temp)
        
        return objects
    
    def get_pull_list_issues(self, _id):
        query = '''SELECT i.id, i.series_id, i.title, i.issue_number, i.comic_vine_id, i.url, i.publish_date,
            i.image_url,i.status, t.title as series_title, t.comic_vine_id as cv_id
            FROM collection_issue as i INNER JOIN collection_title as t 
            ON i.series_id = t.id 
            WHERE i.series_id = %s
            AND i.comic_vine_id IS NOT NULL
            ORDER BY issue_number ASC
        '''
        parm = [_id]
        issuelist = []
        result = self.connection.execute_select(query, parm)
        for issue in result:
            issuelist.append({"id": issue[0], "series_id": issue[1], "title": issue[2],
                               "issue_number": issue[3], "url": issue[5], "comic_vine_id": issue[4],
                               "publish_date": issue[6], "image_url": issue[7],
                               "status": issue[8], "series_title": issue[9], "cv_id": issue[10] })
        
            
        return issuelist
    
    #--- REPORT METHODS
    
    def get_missing_issues(self):
        query = '''SELECT i.id, i.series_id, i.title, i.issue_number, i.comic_vine_id, i.url, i.publish_date,
            i.image_url, i.status, t.title as series_title, t.comic_vine_id as cv_id
            FROM collection_issue as i INNER JOIN collection_title as t 
            ON i.series_id = t.id 
            WHERE STATUS < 8
            ORDER BY t.title
            '''
        result = self.connection.execute_select(query)
        issuelist = []
        for issue in result:
            issuelist.append({"id": issue[0], "series_id": issue[1], "title": issue[2],
                               "issue_number": issue[3], "url": issue[5], "comic_vine_id": issue[4],
                               "publish_date": issue[6].strftime("%b %Y"), "image_url": issue[7],
                               "status": issue[8], "series_title": issue[9], "cv_id": issue[10] })
            
        return issuelist;
    
    def get_latest_issues(self):
        query = '''SELECT i.id, i.series_id, i.title, i.issue_number, i.comic_vine_id, i.url, i.publish_date,
            i.image_url, i.status, t.title as series_title, t.comic_vine_id as cv_id
            FROM collection_issue i INNER JOIN collection_title as t 
            ON i.series_id = t.id 
            where i.publish_date IS NOT NULL AND i.publish_date >= %s AND i.is_read = FALSE
            ORDER BY CAST(i.issue_number as SIGNED) ASC, i.issue_number ASC
            '''
        today = datetime.today()
        param = [datetime.strftime(datetime(today.year, today.month, 1), '%Y-%m-%d')]
        result = self.connection.execute_select(query, param)
        issuelist = []
        for issue in result:
            issuelist.append({"id": issue[0], "series_id": issue[1], "title": issue[2],
                               "issue_number": issue[3], "url": issue[5], "comic_vine_id": issue[4],
                               "publish_date": issue[6].strftime("%b %Y"), "image_url": issue[7],
                               "status": issue[8], "series_title": issue[9], "cv_id": issue[10] })
        return issuelist
    
    #--- NNTP METHODS
    
    def add_group(self, group):
        query = "INSERT INTO newsgroup (name, size) VALUES (%s, %s)";
        parm = [group["group"], group["size"]]
        return self.connection.execute_insert(query, parm)
    
    def get_groups(self):
        query = "SELECT * FROM newsgroup";
        result = self.connection.execute_select(query)
        groups = []
        for row in result:
            groups.append({"group": row[1], "size": row[0]})
        
        return groups
    
    def get_group_setting(self, group_name):
        sql = "select * from group_setting WHERE name = %s"
        parm = [group_name]
        result = self.connection.execute_select(sql, parm)
        for resultrow in result:
            return {"name": resultrow[0], "last": resultrow[1]}
        
        return None
            
                           
    def upsert_group_info(self, group_info):
        sql = "SELECT count(*) FROM group_setting WHERE name = %s"
        parm = [group_info["name"]]
        result = self.connection.execute_scalar(sql, parm)[0]
        
        if result > 0:
            sql = "UPDATE group_setting SET last_article = %s WHERE name = %s"
            parm = [group_info['last'], group_info['name']]
            self.connection.execute_update(sql, parm)
        else:
            sql = "INSERT INTO group_setting (name, last_article) VALUES (%s,%s)"
            parm = [group_info['name'], group_info['last']]
            self.connection.execute_insert(sql, parm)
        
        
    def clear_groups(self):
        self.connection.execute_query('''DROP TABLE newsgroup''')
        self.connection.execute_query('''CREATE TABLE IF NOT EXISTS group
            ( id INTEGER PRIMARY KEY AUTO_INCREMENT,
                name TEXT NOT NULL,
                size INT NOT NULL)
            ''')

    
class SqliteConnector:
    
    def __init__(self, filename):
        self.connection = SqLiteDataAccess(filename)
    
    def clear_database(self):
        self.connection.execute_query("DELETE FROM collection_title")
        self.connection.execute_query("DELETE FROM collection_issue")
        self.connection.execute_query("DELETE FROM pull_list")
        self.connection.execute_query("DELETE FROM diamond_title")
    
    def initialize_database(self, truncate_tables):
        self.connection.execute_query(''' 
            CREATE TABLE IF NOT EXISTS collection_title (
                id INTEGER PRIMARY KEY ASC AUTOINCREMENT,
                title TEXT NOT NULL,
                volume INTEGER,
                publisher_id INTEGER,
                start_year INTEGER,
                image_url TEXT,
                icon_url TEXT,
                series_type INTEGER NOT NULL,
                comic_vine_id INTEGER,
                in_pull_list INTEGER NOT NULL
            );''')
            
        self.connection.execute_query('''     
            CREATE TABLE IF NOT EXISTS collection_issue (
                id INTEGER PRIMARY KEY ASC AUTOINCREMENT,
                series_id INTEGER NOT NULL,                
                title TEXT,
                issue_number TEXT NOT NULL,
                url TEXT,
                issue_type INTEGER NOT NULL,
                is_digital INTEGER NOT NULL,
                comic_vine_id INTEGER,                
                publish_date TEXT,
                image_url TEXT,
                icon_url TEXT,
                image NONE,
                thumb NONE,
                status INTEGER NOT NULL,
                is_read INTEGER
            );''')

        self.connection.execute_query('''
            CREATE TABLE IF NOT EXISTS collection_publisher (
                id INTEGER PRIMARY KEY ASC AUTOINCREMENT,
                name TEXT
            );''')


        self.connection.execute_query('''CREATE TABLE IF NOT EXISTS diamond_title (
                id INTEGER PRIMARY KEY ASC AUTOINCREMENT,
                title VARCHAR(100) NOT NULL,
                price REAL NOT NULL,
                publisher VARCHAR(100),
                pub_date VARCHAR(100),
                code VARCHAR(10) NOT NULL
            );''')
        
        
        
        self.connection.execute_query('''CREATE TABLE IF NOT EXISTS newsgroup
            (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                name TEXT NOT NULL,
                size INT NOT NULL)
            ''')
        
        self.connection.execute_query('''CREATE TABLE IF NOT EXISTS group_setting(
                                         name TEXT PRIMARY KEY,
                                         last_article INTEGER NOT NULL)''')
        
        self.connection.execute_query('''
          CREATE TABLE IF NOT EXISTS article(
                    id INTEGER PRIMARY KEY ASC AUTOINCREMENT,
                    subject TEXT NOT NULL,
                    parts TEXT,
                    total_parts INT,
                    complete TINYINT,
                    filename TEXT,
                    groups TEXT,
                    poster TEXT,
                    date_posted INT,
                    size INT,
                    yenc TINYINT
                );''')
        
        self.connection.execute_query('CREATE INDEX IF NOT EXISTS filename_asc ON article (filename ASC)')
        self.connection.execute_query('CREATE INDEX IF NOT EXISTS status_asc ON collection_issue (status ASC)')
        self.connection.execute_query('CREATE INDEX IF NOT EXISTS collection_asc ON collection_title (title ASC)')
        
        
        if truncate_tables:
            self.clear_database()

    def export_read_issues(self):
        sql="SELECT s.title, s.volume, i.issue_number FROM collection_title s INNER JOIN collection_issue i on i.series_id = s.id WHERE i.is_read = 1";
        pass

    def clear_diamond_table(self):
        self.connection.execute_query("DROP TABLE diamond_title")
            
    def check_diamond_title_exists(self, title):
        sql = '''
         SELECT COUNT(id) from diamond_title WHERE title = ":title"
        '''
        parm = {"title": title }
        
        result = self.connection.execute_scalar(sql, parm)
        if result[0] > 0:
            return True
        else:
            return False
        
            
    def add_available_title(self, title):
        sql = '''
            INSERT INTO diamond_title
            (title, price, code, pub_date, publisher)
            VALUES
            (:title, :price, :code, :publish_date, :publisher)
        '''
        self.connection.execute_insert(sql, title)
            
    def get_collection_size(self):
        sql = ''' 
         SELECT count(id) FROM collection_title
        '''
        title_count = self.connection.execute_scalar(sql)[0]
        
        sql = ''' 
         SELECT count(id) FROM collection_issue
        '''
        issue_count = self.connection.execute_scalar(sql)[0]
        
        return ( title_count, issue_count)

    #--- Publisher methods

    def get_publisher_exists(self, publisher_name):
        sql = "SELECT * FROM collection_publisher where name = :pub_name"
        param = {"pub_name": publisher_name}

        result = self.connection.execute_select(sql, param)

        try:
            pub = result.next()
            print pub
            return pub
        except:
            return False

    def insert_publisher(self, publisher_name):
        sql = ''' INSERT INTO collection_publisher (name) VALUES (:pub_name)'''
        result = self.connection.execute_insert(sql, {"pub_name": publisher_name})

        return result

    def get_collection_publishers(self):
        sql = "SELECT * FROM collection_publisher"
        result = self.connection.execute_select(sql)

        objects = list()

        for data in result:
            obj = {"id": data[0], "name": data[1]}
            objects.append(obj)
        return objects

    def get_collection_titles(self, data):
        series_types = []
        if data['types']['regular'] is True:
            series_types.append("'Ongoing'")

        if data['types']['mini'] is True:
            series_types.append("'Mini-Series'")

        if data['types']["one_shot"] is True:
            series_types.append("'One-Shot'")

        sql = '''
        SELECT id, title, volume, comic_vine_id, publisher_id FROM collection_title
        WHERE series_type IN ('''

        sql += ', '.join(series_types)

        sql += ''')
             AND publisher_id IN ('''

        publishers = []
        for obj in data['publishers']:
            if obj['active'] is True:
                publishers.append(obj['publisher_id'])

        sql += ', '.join(str(v) for v in publishers)

        sql += ''')
        order by title, volume;
        '''
        result = self.connection.execute_select(sql)

        return result

    def get_collection_issues(self, series_id, page=1, rowcount = 20, sortcol = 'issue_number', sortord = 'ASC', issue_type = IssueType.NORMAL):
        sql = '''
        SELECT id, title, issue_number, url, is_read, issue_type, icon_url, comic_vine_id FROM collection_issue
        WHERE series_id = :series
        '''
        if issue_type != IssueType.NORMAL:
            sql += '''AND issue_type  & :type '''
        else:
            sql += "AND issue_type = :type " 
        
        sql += '''
        AND status = 8
        ORDER BY CAST(issue_number as REAL) ASC, issue_number ASC
        LIMIT :rowcount OFFSET :offset
        '''
        params = {"series": series_id, "column": sortcol, "rowcount": rowcount, "offset": (page - 1) * rowcount, "type": issue_type}
        result = self.connection.execute_select(sql, params)
        
        if issue_type != IssueType.NORMAL:
            sql = "SELECT count(id) FROM collection_issue WHERE series_id = ? AND issue_type & ?"
        else:
            sql = "SELECT count(id) FROM collection_issue WHERE series_id = ? AND issue_type = ?"
        
        count = self.connection.execute_scalar(sql, (series_id, issue_type))[0]
        
        if issue_type != IssueType.NORMAL:
            sql = "SELECT count(id) FROM collection_issue WHERE series_id = ? AND issue_type & ? AND status = 8"
        else:
            sql = "SELECT count(id) FROM collection_issue WHERE series_id = ? AND issue_type = ? AND status = 8"
        
        in_collection = self.connection.execute_scalar(sql, (series_id, issue_type))[0]
        
        objects = list()
        index = 1
        for data in result:
            obj = {"id": data[0],"issueNumber": data[2], "url": data[3], "icon_url": data[6],
                    "title": data[1], "isRead": data[4], "index": index, 'issue_type': data[5], 'comic_vine_id': data[7]}
            objects.append(obj)

        total = (count / rowcount)
        if count % rowcount != 0:
            total = total + 1
        
        return {"page": page, "rows": objects, "records": count, "in_collection": in_collection, "total_pages": total}
        
            
    def collection_issue_exists(self, series_id, issue_number, issue_type, issue_url = None ):
        sql = '''
            SELECT count(id) FROM collection_issue
            WHERE series_id = :id 
            AND issue_number = :num
            AND status = 8
            
            '''
        if issue_type != IssueType.NORMAL:
            sql += '''AND issue_type  & :type '''
        else:
            sql += "AND issue_type = :type "
            
        params = {"id": series_id, "num": issue_number, "type": issue_type }
            
        if issue_url is not None:
            sql += "AND url = :url"
            params["url"] = issue_url  
        
        
        result = self.connection.execute_scalar(sql, params)
        if result[0] > 0:
            return True
        else:
            return False
        
    def collection_title_exists(self, title, volume ):
        query = '''
        SELECT count(id) as num, id FROM collection_title
        WHERE title = ? 
        AND volume = ?;
        '''
        params = (title, volume)
        result = self.connection.execute_scalar(query, params)
        if result[0] > 0:
            return result[1]
        else:
            return None
        
            
    def add_collection_title(self, title):
        sql = '''
            INSERT INTO collection_title (title, volume, in_pull_list, series_type, publisher_id)
            VALUES
            (:title, :vol, :in_pull, :series_type, :pub_id);
            '''
        params = { "title": title["title"],"vol": title["volume"], "in_pull": False, 'series_type': title["series_type"], 'pub_id': title['publisher_id']}
        return self.connection.execute_insert(sql, params)[0] 


    def add_collection_issue(self, issue):
        '''
            @param ComicIssue: issue
        '''
        sql ='''
            INSERT INTO collection_issue
            (series_id, issue_number, url, issue_type, is_digital, publish_date, image, thumb, is_read, status)
            VALUES
            (:series_id, :issue_number, :url,:issue_type, :is_digital, :publish_date, :image, :thumb, :is_read, :status);
        '''
        parameters = {"series_id": issue.series_id,"issue_number": issue.issue_number, "url": issue.url, "issue_type": issue.issue_type, "is_digital": issue.is_digital,
                      "publish_date": issue.publish_year, "image": issue.image, "thumb": issue.thumb, "is_read": issue.is_read, "status": 8}
        self.connection.execute_insert(sql, parameters)
        
    def update_issue_read(self, issue_id, is_read):   
        sql = '''
            UPDATE collection_issue SET
            is_read = ?
            WHERE id = ?;
            '''
        
        params = (is_read, issue_id);
        self.connection.execute_update(sql, params);
        
    def get_thumb(self, issue_id):
        query = '''
            SELECT thumb FROM collection_issue
            WHERE id = ?
            '''
        result = self.connection.execute_select(query, [issue_id] )
        return result.next()
    
    def get_image(self, issue_id = None):
        query = '''
            SELECT image FROM collection_issue
            WHERE id = :id
            '''
        if issue_id is None or issue_id == "null":
            f = open('webslinger/static/images/ImageNotAvailable.jpg', 'rb')
            buff = f.read()
            f.close()
            return buff
        try:
            result = self.connection.execute_select(query,{"id": issue_id})
            return result.next()
        except:
            f = open('webslinger/static/images/ImageNotAvailable.jpg', 'rb')
            buff = f.read()
            f.close()
            return buff
    
    
    def add_pull_list_title(self, title):
        sql = '''
            INSERT INTO collection_title 
            (title,  publisher, series_type, image_url, comic_vine_id, in_pull_list)
            VALUES
            (:title, :publisher, :series_type, :image_url,  :comic_vine_id, 1)
        '''
        title = title.__dict__;
        params ={"title": title["title"],  "publisher": title["publisher"], "series_type": title["series_type"], "image_url": title["image_url"], "comic_vine_id": title["comic_vine_id"]}
        series_id = self.connection.execute_insert(sql, params)
        self._get_issues_from_cv(title["comic_vine_id"], series_id[0])
        
    def map_series_to_pull_list(self,title ):
        sql ='''
            UPDATE collection_title 
            SET 
            comic_vine_id = :cv_id, 
            publisher = :publisher,
            image_url = :image_url,
            in_pull_list = 1
            WHERE id = :series_id'''
        self.connection.execute_update(sql, title)
        collector = ComicVineUpdateService(self, title["series_id"], title["cv_id"])
        collector.start()
        
    def insert_comic_vine_issue(self, series_id, issue_obj):
        sql ='''
                    INSERT INTO collection_issue (series_id, title, issue_number,issue_type, is_digital, status, comic_vine_id, publish_date, is_read, image_url, icon_url)
                    VALUES
                    (:series_id,:title, :issue_number, :issue_type, :is_digital, :status, :comic_vine_id, :publish_date, :is_read, :image, :icon)
                '''
        parm = {"series_id": series_id,"title": issue_obj["name"], "issue_number": issue_obj["issue_number"], "issue_type": 1,"is_digital": True,"status": 1,\
                "comic_vine_id": issue_obj["id"],"publish_date": issue_obj["cover_date"], "is_read": False,
                 "image": issue_obj["image"]["super_url"], "icon": issue_obj["image"]["thumb_url"]}
        self.connection.execute_insert(sql, parm)
        
     
    def update_issue_with_comic_vine_info(self, series_id, issue_obj):
        sql = '''
                        UPDATE collection_issue
                        SET 
                        title = :title,
                        
                        is_digital = :is_digital,
                        status = 8,
                        comic_vine_id = :comic_vine_id,
                        publish_date = :publish_date,
                        image_url = :image,
                        icon_url = :icon
                        WHERE series_id = :series_id
                        AND issue_number = :issue_number
                        AND issue_type = :issue_type
                    '''
        parm = {"series_id": series_id,"title": issue_obj["name"], "issue_number": issue_obj["issue_number"], "issue_type": 1,"is_digital": True,\
                            "comic_vine_id": issue_obj["id"],"publish_date": issue_obj["cover_date"], "is_read": False,
                             "image": issue_obj["image"]["super_url"], "icon": issue_obj["image"]["thumb_url"]}
        self.connection.execute_update(sql, parm)
       
    def _get_issues_from_cv(self, _id, series_id):
        collector = ComicVineInsertService(self,series_id, _id);
        collector.start()
          
        
    
    def delete_pull_title(self, _id):
        sql = '''UPDATE  collection_title SET in_pull_list = 0  WHERE id = ?'''
        self.connection.execute_insert(sql, [_id])    
    
    def get_current_pull_list(self, page = 1, rows = 10, sortcol = "title", sortord = 1):
        sql = '''
        SELECT * FROM collection_title WHERE in_pull_list = 1 order by publisher, title ;
        '''
        #params = {"rows": rows, "offset": (page -1) * rows }
        result = self.connection.execute_select(sql)
        objects = []
        for row in result:
            temp = {"id": row[0], "title": row[1], "volume": row[2], "publisher": row[3], 
             "image_url": row[5], "comic_vine_id": row[7] }
            objects.append(temp)
        
        return objects
    
    def get_pull_list_issues(self, _id):
        query = '''SELECT i.id, i.series_id, i.title, i.issue_number, i.comic_vine_id, i.url, i.publish_date,
            i.image_url,i.status, t.title as series_title, t.comic_vine_id as cv_id
            FROM collection_issue as i INNER JOIN collection_title as t 
            ON i.series_id = t.id 
            WHERE i.series_id = :series
            AND i.comic_vine_id NOT NULL
            ORDER BY issue_number DESC
        '''
        parm = {"series": _id};
        issuelist = []
        result = self.connection.execute_select(query, parm)
        for issue in result:
            issuelist.append({"id": issue[0], "series_id": issue[1], "title": issue[2],
                               "issue_number": issue[3], "url": issue[5], "comic_vine_id": issue[4],
                               "publish_date": issue[6], "image_url": issue[7],
                               "status": issue[8], "series_title": issue[9], "cv_id": issue[10] })
            #issuelist.append(issue)
            
        return issuelist
    
    def get_missing_issues(self):
        query = '''SELECT i.id, i.series_id, i.title, i.issue_number, i.comic_vine_id, i.url, i.publish_date,
            i.image_url,i.status, t.title as series_title, t.comic_vine_id as cv_id
            FROM collection_issue as i INNER JOIN collection_title as t 
            ON i.series_id = t.id 
            WHERE STATUS < 8
            ORDER BY t.title ASC, i.issue_number ASC;
            '''
        result = self.connection.execute_select(query)
        issuelist = []
        for issue in result:
            issuelist.append({"id": issue[0], "series_id": issue[1], "title": issue[2],
                               "issue_number": issue[3], "url": issue[5], "comic_vine_id": issue[4],
                               "publish_date": issue[6], "image_url": issue[7],
                               "status": issue[8], "series_title": issue[9], "cv_id": issue[10] })
            
        return issuelist;
    
    def get_latest_issues(self):
        query = '''SELECT i.id, i.series_id, i.title, i.issue_number, i.comic_vine_id, i.url, i.publish_date,
            i.image_url, i.status, t.title as series_title, t.comic_vine_id as cv_id
            FROM collection_issue i INNER JOIN collection_title as t 
            ON i.series_id = t.id 
            where i.publish_date IS NOT NULL AND i.publish_date >= ? AND i.is_read = 0
            ORDER BY series_title ASC, CAST(i.issue_number as SIGNED) ASC, i.issue_number ASC
            '''
        today = datetime.today()
        param = [datetime.strftime(datetime(today.year, today.month, 1), '%Y-%m-%d')]
        result = self.connection.execute_select(query, param)
        issuelist = []
        for issue in result:
            issuelist.append({"id": issue[0], "series_id": issue[1], "title": issue[2],
                               "issue_number": issue[3], "url": issue[5], "comic_vine_id": issue[4],
                               "publish_date": issue[6].strftime("%b %Y"), "image_url": issue[7],
                               "status": issue[8], "series_title": issue[9], "cv_id": issue[10] })
        return issuelist
    
    def get_is_in_pull(self, _id):
        query = ''' SELECT in_pull_list FROM collection_title WHERE id = :series_id'''
        
        params = {"series_id": _id}
        try:
            result = self.connection.execute_select(query, params).next();
            return result;
        except:
            return None
    
    def add_group(self, group):
        query = "INSERT INTO newsgroup (name, size) VALUES (:group, :size)";
        parm = {"group": group["group"], 'size': group["size"]}
        return self.connection.execute_insert(query, parm)
    
    def get_groups(self):
        query = "SELECT * FROM newsgroup";
        result = self.connection.execute_select(query)
        groups = []
        for row in result:
            groups.append({"group": row[1], "size": row[0]})
        
        return groups
    
    def get_group_setting(self, group_name):
        sql = "select * from group_setting WHERE name = :name"
        parm = {"name": group_name}
        result = self.connection.execute_select(sql, parm)
        for resultrow in result:
            return {"name": resultrow[0], "last": resultrow[1]}
        
        return None
            
                           
    def upsert_group_info(self, group_info):
        sql = "SELECT count(*) FROM group_setting WHERE name = :name"
        parm = group_info
        result = self.connection.execute_scalar(sql, parm)[0]
        
        if result > 0:
            sql = "UPDATE group_setting SET last_article = :last WHERE name = :name"
            parm = group_info
            self.connection.execute_update(sql, parm)
        else:
            sql = "INSERT INTO group_setting (name, last_article) VALUES (:name,:last)"
            parm = group_info
            self.connection.execute_insert(sql, parm)
        
        
    def clear_groups(self):
        self.connection.execute_query('''DROP TABLE newsgroup''')
        self.connection.execute_query('''CREATE TABLE IF NOT EXISTS group
            ( id INTEGER PRIMARY KEY AUTOINCREMENT,
                name TEXT NOT NULL,
                size INT NOT NULL)
            ''')
    
if __name__ == '__main__':
    print("Cannot execute directly.")