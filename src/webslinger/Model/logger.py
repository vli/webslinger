# Author: Brett Vann <brett.vann@vannluminous.com>
# URL: http://github.com/roninbv/webslinger/
#
# This file is part of Web Slinger.
#
# Web Slinger is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Web Slinger is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Web Slinger.  If not, see <http://www.gnu.org/licenses/>.
#
import logging
from logging.handlers import RotatingFileHandler


class WSlingerLogger(object):
  
    def __init__(self, logfile_name, logging_level):
        
        handler = RotatingFileHandler(logfile_name, maxBytes = 102400, backupCount = 3)
        cons = logging.StreamHandler()
        cons.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', '%m/%d/%Y %I:%M:%S %p')
        handler.setFormatter(formatter)
        self.logger = logging.getLogger("ws_logger")
        self.logger.setLevel(logging_level)
        self.logger.addHandler(handler)
        self.logger.addHandler(cons)
    
    def log_warning(self, warning):
        self.logger.warning(warning)
    
    def log_error(self, err_message):
        self.logger.error(err_message)
    
    def log_info(self, info_message):
        self.logger.info(info_message)
    
    def log_exception(self, exception):
        self.logger.exception(exception)
        
    def log_debug(self, debug_message):
        self.logger.debug(debug_message)
        