# Author: Brett Vann <brett.vann@vannluminous.com>
# URL: http://github.com/roninbv/webslinger/
#
# This file is part of Web Slinger.
#
# Web Slinger is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Web Slinger is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Web Slinger.  If not, see <http://www.gnu.org/licenses/>.

ish_regexes = [
            ('zero_day',
             '''
             ^0[- _.]\D*[- _.]+"          # 0-day
             (?P<comic_title>\D*)[-. _]
             (?P<issue_num>\d*)[-. _\(]+
             (?P<year>\d*)[-. _\)\(]+
             (?P<group>\D+)\)[-. _]
             (?P<extension>cb[rz] | rar | zip)              
             '''
             ),
            ('complete',
             '''
             ^(?P<comic_title>.+?)[. _-]+            # Comic Title
             [Vv]ol[. _-](?P<volume>\d+)[. _-]+            # Volume
             [Nn]o[. _-](?P<issue_num>\d+)[. _-]+
             (?P<month>\D+)[. _-]+
             (?P<year>\d+)[. _-]+
             (?P<group>\D+)[. _-]+
             (?P<extension>cb[rz] | rar | zip).              
             '''
             ),
             
            ('no_month',
             '''
             ^(?P<comic_title>.+?)[. _-]+            # Comic Title
             (?P<issue_num>\d+)[. _-]+
             [(](?P<year>\d+)[)][. _-]+
             [(](?P<group>\D+)[)][. _-]+
             (?P<extension>cb[rz] | rar | zip)              
             '''
             ),
             
            ('short',
             '''
             ^(?P<comic_title>.+?)[#. _-]+            # Comic Title
             (?P<issue_num>\d+)[. _-]+
             (?P<extension>cb[rz] | rar | zip)              
             '''
             ),
             
            ('dumb',
             '''
             ^(?P<comic_title>.+?)[. _-]+            # Comic Title
             (?P<issue_num>\d+)[. _-]+
             (?P<year>\d\d\d\d)
             .*[.](?P<extension>cb[rz] | rar | zip)[. _-]+              
             '''
            ),
            
            ('quoted',
            '''
             .*"(?P<comic_title>.+?)[. _-]+?        # Comic Title and separator
             (?P<issue_num>\d+)[#. _-]+
             '''
             ),
             
        ]
