# Author: Brett Vann <brett.vann@vannluminous.com>
# URL: http://github.com/roninbv/webslinger/
#
# This file is part of Web Slinger.
#
# Web Slinger is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Web Slinger is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Web Slinger.  If not, see <http://www.gnu.org/licenses/>.
#

# Initialize the webslinger package.
import cherrypy
import logging
import os

from ConfigParser import SafeConfigParser
from threading import Lock
from webslinger.Controllers.home import HomePageController
from webslinger.Model.db import DataAccessFactory
from webslinger.Model.logger import WSlingerLogger
from webslinger.Model.schedulers import Scheduler
from webslinger.Services.collection_service import CollectionService
from webslinger.Services.diamond_indexer import DiamondService
from webslinger.Services.nntp_indexer import ArticleGrabber, ArticleWorker

__INITIALIZED__ = False

LOCK = Lock()

ws_command = None

FULLNAME = None
CONFIG_FILE = None
CFG_OBJ = None
PROG_DIR = '.'
CREATEPID = False
PIDFILE = ''
DAEMON = False

LOG_DIR = 'logs'
LOG_FILE = "webslinger.log"
LOG_LEVEL = logging.INFO
LOGGER = None

# CHERRYPY SETTINGS
CHERRYPY_PORT = 50503
CHERRYPY_HOST = '0.0.0.0'
CHERRYPY_TRAILING_SLASH = False
CHERRYPY_STATIC_DIR_ON = True
CHERRYPY_STATIC_DIR = "webslinger/static"


# COMIC VINE SETTINGS
COMIC_VINE_API_KEY = "21bd63c504a1023da5665835be7c17dacd02cd90"
COMIC_VINE_BASE_URL = "http://api.comicvine.com/"

# DIGITAL COLLECTION SETTINGS
COLLECTION_PATH = None
COLLECTION_SERVICE_ACTIVE = True
COLLECTION_RESCAN_FREQ = 5
COLLECTION_RESCAN_DENOM = 2


#DIAMOND SETTINGS
DIAMOND_SERVICE_ACTIVE = False
DIAMOND_REFRESH_FREQ = 1
DIAMOND_REFRESH_DENOM = 4
DIAMOND_RECORD_WEEKS = 4


# DATABASE SETTINGS
DB_TYPE = 1
DB_HOST_OR_FILE = "webslinger.sqlite"
DB_PORT = None
DB_NAME = None
DB_USER = None
DB_PASS = None
DB_CONNECTION = None
ARTICLE_DB = "articles.sqlite"
ARTICLE_CONNECTION = None

# USENET SETTINGS
NNTP_SERVICE_ACTIVE = False
NNTP_SERVER = None
NNTP_PORT = 119
NNTP_USER = None
NNTP_PASSWORD = None
NNTP_MAX_CONNECTIONS = 4
NNTP_USE_SSL = False
NNTP_RETENTION = 500


# THREADS
collection_indexer = None
diamond_indexer = None
article_worker = None
article_producer = None
backIssueSearchScheduler = None
currentIssueSearchScheduler = None


def initialize(consoleLog = True):
    
    with LOCK:
        
        global __INITIALIZED__, CFG_OBJ, CONFIG_FILE, \
            DB_TYPE, DB_HOST_OR_FILE, DB_PORT, DB_NAME, DB_USER, DB_PASS, DB_CONNECTION, ARTICLE_DB, ARTICLE_CONNECTION,\
            COLLECTION_SERVICE_ACTIVE, COLLECTION_PATH, COLLECTION_RESCAN_FREQ, COLLECTION_RESCAN_DENOM,\
            NNTP_SERVER, NNTP_PORT, NNTP_USER, NNTP_PASSWORD, NNTP_MAX_CONNECTIONS, NNTP_USE_SSL, NNTP_RETENTION, NNTP_SERVICE_ACTIVE,\
            LOGGER, LOG_DIR, LOG_LEVEL, LOG_FILE, DB_CONNECTION,\
            collection_indexer, diamond_indexer, article_worker, article_producer


        if __INITIALIZED__:
            return False
        
        CFG_OBJ = SafeConfigParser()
        CFG_OBJ.read(CONFIG_FILE)    
        LOGGER = WSlingerLogger(os.path.join(LOG_DIR, LOG_FILE), LOG_LEVEL)
        
        LOGGER.log_info("Initializing Webslinger...")
    
        LOGGER.log_info("Initializing Databases...")
        
        
        
        
        DB_TYPE = CFG_OBJ.getint("database", "data_store")
        DB_HOST_OR_FILE = CFG_OBJ.get("database", "host").strip('"').strip("'")
        DB_PORT = CFG_OBJ.getint("database", "port")
        DB_NAME = CFG_OBJ.get("database", "name").strip('"').strip("'")
        DB_USER = CFG_OBJ.get("database", "db_user").strip('"').strip("'")
        DB_PASS = CFG_OBJ.get("database", "db_pass").strip('"').strip("'")
    
        factory = DataAccessFactory()
        DB_CONNECTION = factory.get_db_connector(DB_TYPE, DB_HOST_OR_FILE, DB_NAME, DB_USER, DB_PASS)
        print DB_CONNECTION
        DB_CONNECTION.initialize_database(False)
        
        
        LOGGER.log_info("Database init complete.")
        
        LOGGER.log_info("Initializing CherryPy...")
        
        init_web_server()
        
        LOGGER.log_info("CherryPy init complete.")
        
        LOGGER.log_info("Initializing Services...")
        
        COLLECTION_SERVICE_ACTIVE = CFG_OBJ.getboolean("digital_collection", "service_active")
        COLLECTION_PATH = CFG_OBJ.get("digital_collection", "path").strip("'").strip('"');
        COLLECTION_RESCAN_FREQ = CFG_OBJ.getint("digital_collection", "rescan_freq")
        COLLECTION_RESCAN_DENOM = CFG_OBJ.getint("digital_collection", "rescan_denom")
        
        
        NNTP_SERVER = CFG_OBJ.get("nntp", "host").strip('"').strip("'")
        NNTP_PORT = CFG_OBJ.getint("nntp", "port")
        NNTP_USER = CFG_OBJ.get("nntp", "nntp_user").strip('"').strip("'")
        NNTP_PASSWORD = CFG_OBJ.get("nntp", "nntp_password").strip('"').strip("'")
        NNTP_MAX_CONNECTIONS = CFG_OBJ.getint("nntp", "max_threads")
        NNTP_USE_SSL = CFG_OBJ.getboolean("nntp", "use_ssl")
        NNTP_SERVICE_ACTIVE = CFG_OBJ.getboolean("nntp", "service_active")
        
        collection_indexer = Scheduler(CollectionService(DB_CONNECTION), determine_cycle_time(COLLECTION_RESCAN_FREQ, COLLECTION_RESCAN_DENOM),
                                       "COLLECTION_INDEXER", True, False)
        diamond_indexer = Scheduler(DiamondService(DB_CONNECTION), determine_cycle_time(DIAMOND_REFRESH_FREQ, DIAMOND_REFRESH_DENOM),
                                   "DIAMOND_INDEXER", False,False)
        
        
    
        #article_producer = Scheduler(ArticleGrabber(),60*5, "ARTICLE_GRABBER", True, False )
        
    
        #article_worker = Scheduler(ArticleWorker(article_producer.action), .1, "ARTICLE_WORKER", True, False)
        
        
        LOGGER.log_info("Services init complete.")
        __INITIALIZED__ = True
        return True
    
def determine_cycle_time(freq, denom):
    #minutes
    if denom == 1:
        return freq
    #hours
    if denom == 2:
        return freq * 60
    #days
    if denom ==  4:
        return freq * 60 * 24

def start():
    
    global __INITIALIZED__, collection_indexer, diamond_indexer
    
    with LOCK:
        
        if __INITIALIZED__:
            LOGGER.log_info("Starting all webslinger services...")
            collection_indexer.thread.start()
            diamond_indexer.thread.start()
            #~ article_producer.thread.start()
            #~ article_worker.thread.start()
            LOGGER.log_info("Start up complete. Entering main loop...")
        

def halt():
    global __INITIALIZED__, collection_indexer, diamond_indexer
    
    with LOCK:
        if __INITIALIZED__:
    
            collection_indexer.abort = True;
            try:
                collection_indexer.thread.join(10)
            except:
                pass
            
            
            diamond_indexer.abort = True;
            try:
                diamond_indexer.thread.join(10)
            except:
                pass
            
            
            #~ article_worker.abort = True
            #~ try:
                #~ article_worker.join(10)
            #~ except:
                #~ pass
            
            #~ article_producer.abort = True
            #~ try:
                #~ article_producer.join(10)
            #~ except:
                #~ pass
            
            __INITIALIZED__ = False
            
            
def remove_pid_file(PIDFILE):
    try:
        if os.path.exists(PIDFILE):
            os.remove(PIDFILE)

    except (IOError, OSError):
        return False

    return True


def init_web_server():
    options_dict = {
        'server.socket_port': CHERRYPY_PORT,
        'server.socket_host': CHERRYPY_HOST,
        'tools.staticdir.root': "/home/roninbv/Code/Python/webslinger/src/webslinger",
        '/static': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': "static"
        }
    }
    cherrypy.config.update(options_dict)
    cherrypy.tree.mount(HomePageController(DB_CONNECTION), "/", CONFIG_FILE)
    cherrypy.server.start()
    cherrypy.server.wait()


def signal_handler(signum=None, frame=None):
    if type(signum) != type(None):
        LOGGER.log_info(u"Signal %i caught, saving and exiting..." % int(signum))
        save_and_shut_down()

def save_all():
    pass

def save_and_shut_down(restart = False):
    cherrypy.engine.exit()
    halt()
    save_all() 
    
    if CREATEPID:
        LOGGER.log_info(u"Removing pidfile " + str(PIDFILE))
        remove_pid_file(PIDFILE)
    
    os._exit(0)
    
def invoke_command(to_call, *args, **kwargs):
    global invoked_command
    def delegate():
        to_call(*args, **kwargs)
    invoked_command = delegate
    LOGGER.log_info(u"Placed invoked command: "+repr(invoked_command)+" for "+repr(to_call)+" with "+repr(args)+" and "+repr(kwargs) )

def invoke_restart(soft=True):
    invoke_command(restart, soft=soft)

def invoke_shutdown():
    invoke_command(save_and_shut_down)

def restart(soft = True):
    pass

def save_config():
    pass

