# Author: Brett Vann <brett.vann@vannluminous.com>
# URL: http://github.com/roninbv/webslinger/
#
# This file is part of Web Slinger.
#
# Web Slinger is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Web Slinger is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Web Slinger.  If not, see <http://www.gnu.org/licenses/>.

# This is a service for managing the existing 
# collection in the file system...

import os
import threading
import webslinger
from webslinger.Model.entities import ComicIssue, IssueType, Publisher
from webslinger.Model.parsers import CollectionParser


class CollectionService(threading.Thread):
    def __init__(self, db):
        threading.Thread.__init__(self)
        self.connection = db
        self.active = False
        self.lock = threading.Lock()
        self.collection_parser = CollectionParser()
        self.current_title = (None, None, None)
        self.current_publisher = None
        pass

    def run(self):
        if webslinger.COLLECTION_SERVICE_ACTIVE is False:
            webslinger.LOGGER.log_info("Collection Indexer is disabled. I will check again next cycle")
            return

        if self.active is True:
            webslinger.LOGGER.log_info("Collection Indexer already running. Waiting until the next cycle")
            return

        try:
            self.active = True
            webslinger.LOGGER.log_info("running collection service.")
            self.connection.initialize_database(False)
            webslinger.LOGGER.log_info("starting to parse comics.")
            webslinger.LOGGER.log_info("root: %s" % os.path.abspath(webslinger.COLLECTION_PATH))
            self.parse_collection(webslinger.COLLECTION_PATH)
            collection_count = self.connection.get_collection_size()
            webslinger.LOGGER.log_info("Collection Size: %d titles, totalling %d issues " % collection_count)
            self.active = False

        except AttributeError as ex:
            self.active = False
            webslinger.LOGGER.log_exception(ex)

    def parse_collection(self, file_name):
        out = []
        os.path.walk(file_name, self.implement_walk, out)
        return out

    def is_awake(self):
        webslinger.LOGGER.log_info("active: " + str(self.active))
        return self.active

    def implement_walk(self, arg, path, files):
        arg = 0
        for child in files:
            full_file_path = os.path.join(path, child)
            full_file_path = os.path.normpath(full_file_path)

            if CollectionService.check_file_type(full_file_path) is False:
                continue

            arg += 1
            path_parts = path.split(os.sep)

            issue_num = CollectionParser.find_issue_number(child)
            title = CollectionParser.find_issue_title(path_parts[-1])
            volume = CollectionParser.find_issue_volume(path_parts[-1])
            issue_type = CollectionParser.determine_issue_mask(child)
            release_year = CollectionParser.find_publish_year(child)
            series_type = CollectionParser.find_series_type(path)
            publisher_name = CollectionParser.find_publisher(webslinger.COLLECTION_PATH, path)

            self.add_publisher(publisher_name)

            print self.current_publisher

            self.add_title(title, volume, series_type)

            if issue_type & IssueType.KING_SIZE:
                title += " King-Size"

            if issue_type & IssueType.GIANT_SIZE:
                title += " Giant-Size"

            if issue_type & IssueType.SPECIAL:
                title += " Special"

            if issue_type & IssueType.ANNUAL:
                title += " Annual"

            ish = ComicIssue(self.current_title[1], title, issue_num, full_file_path, volume, release_year,
                             is_digital=True, issue_type=issue_type)

            if self.connection.collection_issue_exists(ish.series_id, ish.issue_number, ish.issue_type,
                                                       ish.url) is False:
                webslinger.LOGGER.log_info("Adding %s" % ish.url)
                full, thumb = self.get_images(ish.url)
                ish.image = full
                ish.thumb = thumb
                self.connection.add_collection_issue(ish)
            else:
                webslinger.LOGGER.log_info("%s %d is already in the database" % (ish.title, ish.issue_number));

    @staticmethod
    def check_file_type(filepath):
        filepath = filepath.lower()
        if ".cbr" in filepath or '.cbz' in filepath:
            return True
        else:
            return False

    def get_images(self, full_path):
        if ".cbr" in full_path or '.CBR' in full_path:
            return self.collection_parser.get_thumbnail_cbr(full_path)
        elif ".cbz" in full_path or '.CBZ' in full_path:
            return self.collection_parser.get_thumbnail_cbz(full_path)
        else:
            webslinger.LOGGER.log_warning("IMAGE NOT FOUND FOR %s" % full_path)
            return None, None

    def add_title(self, issue_title, issue_volume, series_type):
        if issue_title != self.current_title[0] or issue_volume != self.current_title[2]:
            db_title_id = self.connection.collection_title_exists(issue_title, issue_volume)

            if db_title_id is not None:
                title_id = db_title_id
            else:
                title_id = self.connection.add_collection_title(
                    {"title": issue_title, "volume": issue_volume, "series_type": series_type, "publisher_id": self.current_publisher._id})

            self.current_title = (issue_title, title_id, issue_volume)

    def add_publisher(self, publisher_name):
        #check publisher name exists
        exists = self.connection.get_publisher_exists(publisher_name)
        if(exists):
            self.current_publisher = Publisher(exists[1], exists[0])
        else:
            pub = self.connection.insert_publisher(publisher_name)
            self.current_publisher = Publisher(publisher_name, pub[0])


if __name__ == "__main__":
    print("Cannot be run in a standalone manner.")