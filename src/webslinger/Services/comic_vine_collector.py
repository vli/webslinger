# Author: Brett Vann <brett.vann@vannluminous.com>
# URL: http://github.com/roninbv/webslinger/
#
# This file is part of Web Slinger.
#
# Web Slinger is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Web Slinger is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Web Slinger.  If not, see <http://www.gnu.org/licenses/>.
import json
import urllib2
import webslinger
from threading import Thread, Lock;


class ComicVineInsertService(Thread):
    
    def __init__(self, db, series_id, cv_id):
        Thread.__init__(self)
        self.connection = db
        self.series_id = series_id
        self.cv_id = cv_id
        self.active = False
        self.lock = Lock();
        self.waiting = False
    
    def run(self):
        
        if self.active == True:
            webslinger.LOGGER.log_info("CV Indexer already running. Waiting until the next cycle")
            return 
        
        try:
            self.active = True
            self.waiting = False
            webslinger.LOGGER.log_info( "running Comic Vine service.")
            
            url = 'http://api.comicvine.com/issues/?filter=volume:%d&api_key=%s&format=json' % (int(self.cv_id), webslinger.COMIC_VINE_API_KEY)
            json_string =  urllib2.urlopen(url).read();
            json_obj = json.loads(json_string);
            for issue in json_obj["results"]:
                self.connection.insert_comic_vine_issue(self.series_id, issue)
            self.active = False
            self.waiting = True
        except AttributeError as ex:
            self.active = False
            self.waiting = True
            webslinger.LOGGER.log_exception(ex)
        
        webslinger.LOGGER.log_info("List Complete.")

    def is_awake(self):
        webslinger.LOGGER.log_info("active: " + str(self.active) + " waiting: " + self.waiting)
        return (not self.waiting) and self.active
    
    
class ComicVineUpdateService(Thread):
    
    def __init__(self, db, series_id, cv_id):
        Thread.__init__(self)
        self.connection = db
        self.series_id = series_id
        self.cv_id = cv_id
        self.active = False
        self.lock = Lock();
        self.waiting = False
    
    def run(self):
        if self.active == True:
            webslinger.LOGGER.log_info("CV Updater already running. Waiting until the next cycle")
            return 
        
        try:
            self.active = True
            self.waiting = False
            webslinger.LOGGER.log_info( "running Comic Vine Update service.")
            url = 'http://api.comicvine.com/issues/?filter=volume:%d&api_key=%s&format=json' % (int(self.cv_id), webslinger.COMIC_VINE_API_KEY)
            json_string =  urllib2.urlopen(url).read();
            json_obj = json.loads(json_string);
            for issue in json_obj["results"]:                
                if self.connection.collection_issue_exists(self.series_id, issue["issue_number"], 1):
                    self.connection.update_issue_with_comic_vine_info(self.series_id, issue)
                else:
                    self.connection.insert_comic_vine_issue(self.series_id, issue)
            self.active = False
            self.waiting = True
        except AttributeError as ex:
            self.active = False
            self.waiting = True
            webslinger.LOGGER.log_exception(ex)
        webslinger.LOGGER.log_info("List Complete.")

    def is_awake(self):
        webslinger.LOGGER.log_info("active: " + str(self.active) + " waiting: " + self.waiting)
        return (not self.waiting) and self.active