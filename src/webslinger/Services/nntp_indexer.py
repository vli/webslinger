# Author: Brett Vann <brett.vann@vannluminous.com>
# URL: http://github.com/roninbv/webslinger/
#
# This file is part of Web Slinger.
#
# Web Slinger is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Web Slinger is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Web Slinger.  If not, see <http://www.gnu.org/licenses/>.
#
import calendar
import datetime
import itertools
import nntplib
import re
import ssl
import threading


import webslinger
import webslinger.Model.nntp_ssl as nntp_ssl
from webslinger.Model.parsers import ArticleParser
    

class ArticleWorker(threading.Thread):

    def __init__(self, parent):
        threading.Thread.__init__(self)
        self.name = "ArticleWorker-%d" % hash(self)
        self.ev = threading.Event()
        self.parent = parent
        self.connection = None
        self.active = False
        self.lock = threading.Lock()
        self.cursor = None
        self.count = 0
        self.CANCEL = False

    def cancel(self):
        self.CANCEL = True

    def run(self):
        
        if self.active:
            webslinger.LOGGER.log_info("ArticleWorker already running. Not starting another.")
            return
        
        self.connection = webslinger.DB_CONNECTION
        
        self.active = True;
        
        while self.parent.header_count > 0 and not self.CANCEL:
            # each variable is a tuple containing the article number and the value. The list
            # compression filters out the article numbers.
            (subject, _from, message_id, date, lines, group) = self.parent.produce()
            subject = subject[1].strip()
            _from = _from[1]
            message_id = message_id[1]
            date = date[1]
            lines = lines[1]

            try:
                if self.add_segment(subject, _from, message_id, date, lines, group):
                    webslinger.LOGGER.log_info("'%s' was added to the database. " % subject)
            except Exception, e:
                webslinger.LOGGER.log_exception("General failure while processing message. Subject: %s, From: %s, Message ID: %s, Date: %s, Lines: %s, Group: %s" % (subject, _from, message_id, date, lines, group))
                
            

        webslinger.LOGGER.log_info("Article processor has nothing to do. Sleeping.")
        self.active = False;

    def add_segment(self, subject, _from, message_id, date, lines, group):
        parse = ArticleParser("bad.txt")
        if parse.bad_filter(subject):
            webslinger.LOGGER.log_info("Looks Spammy: %s" % subject)
            return False

        try:
            subject = self.utf_decode(subject)
            _from = self.utf_decode(_from)
            message_id = self.utf_decode(message_id)
            group = self.utf_decode(group)
        except UnicodeDecodeError, e:
            webslinger.LOGGER.log_exception("UnicodeDecodeError while trying to decode message_id %s" % message_id)
            return False

        subject_similar = parse.subject_to_similar(subject)
        part_num, total_parts = parse.subject_to_totals(subject)
        filename = parse.subject_to_filename(subject)

        if not filename or not subject_similar or not total_parts:
            webslinger.LOGGER.log_info("\"" + subject + "\" is either a plain usenet post or a subject line that cannot be parsed correctly.")
            return False

        yenc = 1 if parse.subject_to_yenc(subject) else 0
        # UUENCODE: 45 bytes per line
        # yEnc: 128 bytes per line
        try:
            if yenc == 1:
                size = int(lines) * 128
            else:
                size = int(lines) * 45
        except ValueError:
            #occasionally the line info returned from the server will be no good
            size = 0

        result = self.connection.connection.execute_select("SELECT rowid, parts, total_parts, groups FROM article WHERE subject=? LIMIT 1", (subject_similar,) )
        
        try:
            (rowid, parts, total_parts, groups) = result.next()

            groups = groups.split(',')
            
            if group not in groups:
                groups.append(group)
                self.connection.connection.execute_update("UPDATE article SET groups=? WHERE rowid=?", (','.join(groups), rowid))

            if message_id in parts:
                return False
                
            parts = eval(parts)

            parts[ message_id ] = part_num

            complete = 1 if len(parts.keys()) == int(total_parts) else 0

            self.connection.connection.execute_update("UPDATE article SET parts=?, complete=?, size=size+? WHERE rowid=?", (str(parts), complete, size, rowid))
        
        except StopIteration, e:

            poster = _from
            parts = { message_id : part_num }

            try:
                date_posted = self.datetime_to_seconds(date)
            except ValueError, e:
                webslinger.LOGGER.log_exception("Unable to parse %s because bad timestamp. Skipping." % subject)
                return False
            
            complete = 1 if len(parts.keys()) == int(total_parts) else 0

            self.connection.connection.execute_insert("INSERT INTO article " +
                "(subject, parts, total_parts, complete, filename, groups, poster, date_posted, size, yenc) " + 
                "VALUES (?,?,?,?,?,?,?,?,?,?)", 
                (subject_similar, str(parts), total_parts, complete, filename, group, poster, date_posted, size, yenc))
            #print (subject_similar, str(parts), total_parts, complete, filename, group, poster, date_posted, size, yenc)           
        return True
    
    def datetime_to_seconds(self, date):

        # lots of inconsistencies with the format of the date with each article.
        found = re.search(r"(?P<date>\d{1,2} \w\w\w \d\d\d\d \d{1,2}:\d\d:\d\d)", date).group(0)
        try:        
            date_obj = datetime.datetime.strptime(found, "%d %b %Y %H:%M:%S")
        except ValueError, e:
            webslinger.LOGGER.log_exception("Not able to parse date %s. Attempting alternate formats." % date)
            try:
                date_obj = datetime.datetime.strptime(date, "%a, %d %b %Y %H:%M:%S %Z")
            except ValueError, e:
                date_obj = datetime.datetime.strptime(date, "%d %b %Y %H:%M:%S %Z")    
                
        time_tuple = date_obj.timetuple()
        return calendar.timegm(time_tuple)
    
    def utf_decode(self, string_to_decode):

        try:
            return string_to_decode.decode('utf-8', 'replace')
        except UnicodeDecodeError, e:
            webslinger.LOGGER.log_exception("Unicode decode error while decoding %s. Error message: %s" % (repr(string_to_decode), str(e)))
            raise e



class ArticleGrabber(threading.Thread):
    name = "ArticleGrabber"
    buffer_capacity = 20000

    def __init__(self):
        threading.Thread.__init__(self)
        self.ev = threading.Event()
        self.headers = LockedIterator([].__iter__())
        self.header_count = 0
        self.connection = None
        self.abort = False
        self.lock = threading.Lock()

    def check_workload(self):

        webslinger.LOGGER.log_info("Checking workload. %d" % self.header_count)
        while not self.abort:
            if self.header_count > int(ArticleGrabber.buffer_capacity * 1.2):
                webslinger.LOGGER.log_info("Producer Thread has has too many articles (%d). Waiting for Worker to catch up." % self.header_count)
                self.ev.wait(3)
            else:
                webslinger.LOGGER.log_info("Producer Thread has %d articles waiting for processing." % self.header_count)
                break

    def produce(self):
        obj = self.headers.next()
        self.header_count -= 1
        return obj

    def connect(self, host, port, username, password, is_ssl = True, retry = 5):

        if retry < 0: return None

        try:
            webslinger.LOGGER.log_info("Creating a new connection.")
            if is_ssl:
                self.connection = nntp_ssl.NNTP_SSL(host, port, username, password)
                webslinger.LOGGER.log_info("(%s:%s,SSL): %s." % (host, port, self.connection.getwelcome()[:20]) )
            else:
                self.connection = nntplib.NNTP(host, port, username, password)
                webslinger.LOGGER.log_info("(%s:%s): %s..." % (host, port, self.connection.getwelcome()[:20]) )
            return self.connection
        
        except (nntplib.NNTPPermanentError, 
                nntplib.NNTPTemporaryError,
                nntplib.NNTPError,
                nntplib.NNTPProtocolError), error:

            
            webslinger.LOGGER.log_exception("NNTPError. Server response: %s" % error.message)

            if retry == 0:
                raise IOError(error)
            else:
                self.ev.wait(60)
                webslinger.LOGGER.log_info("Retrying connection.")
                return self.connect(host, port, username, password, is_ssl, retry - 1)
            
        except IOError, error:
            if retry == 0:
                raise error
            else:
                webslinger.LOGGER.log_exception("Socket Error: %s" % str(error))
                self.ev.wait(60)
                webslinger.LOGGER.log_info("Retrying connection.")
                return self.connect(host, port, username, password, is_ssl, retry - 1)
    
    def run(self):
        db = webslinger.DB_CONNECTION
        
        
        try:
            enabled = webslinger.NNTP_SERVICE_ACTIVE
            host = webslinger.NNTP_SERVER
            port = webslinger.NNTP_PORT
            username = webslinger.NNTP_USER
            password = webslinger.NNTP_PASSWORD
            is_ssl = webslinger.NNTP_USE_SSL
        except ValueError, e:
            webslinger.LOGGER.log_exception("Invalid port value.")
        
        
        if enabled == False:
            webslinger.LOGGER.log_info(" article service is disabled.")
            return
            
        #get groups info from database.
        groups = db.get_groups()
        
        for group in groups:
            
            #get group setting
            group_setting = db.get_group_setting( group["group"])
            print group_setting
            if group_setting is None:
                group = group["group"]
                index = 0
            else:
                group = group_setting["name"]
                index = int(group_setting["last"])

            #connect to server.
            self.connect(host, port, username, password, is_ssl)

            #select the group
            try:
                (response, count, first, last, name) = self.connection.group(group)
            except nntplib.NNTPTemporaryError, e:
                webslinger.LOGGER.log_exception("Failed to select group %s. Server response: %s" %
                    (group, str(e)))
                webslinger.LOGGER.log_info("Skipping %s." % group)
                continue

            webslinger.LOGGER.log_info('Selecting group %s' % group)
            webslinger.LOGGER.log_info(name + ' has ' + count + ' articles, ' + first + ' - ' + last)

            first = int(first)
            last = int(last)
            low = max(int(index), first)
            high = min(low + ArticleGrabber.buffer_capacity, last)
            
            print (group, low, high, last, response)
            while low < high:
                
                self.check_workload()
                
                print(str(low) + " " + str(high))
                try:
                    (response, subjects) = self.connection.xhdr('subject', '%d-%d' % (low, high) )
                    (response, froms) = self.connection.xhdr('from', '%d-%d' % (low, high) )
                    (response, message_ids) = self.connection.xhdr('message-id', '%d-%d' % (low, high) )
                    (response, dates) = self.connection.xhdr('date', '%d-%d' % (low, high) )
                    (response, lines) = self.connection.xhdr('lines', '%d-%d' % (low, high) )

                    webslinger.LOGGER.log_info("[%s: %09d - %09d (of: %09d), %s]" % (group, low, high, last, response))

                except nntplib.NNTPTemporaryError, e:
                    webslinger.LOGGER.log_exception(
                        "Failed to retrieve articles %d through %d from %s. Server response: %s" % 
                        (low, high, group, str(e)))
                    continue
                except nntplib.NNTPPermanentError, e:
                    webslinger.LOGGER.log_exception(
                        "Failed to retrieve articles %d through %d from %s. Server response: %s" % 
                        (low, high, group, str(e)))
                    continue
                except ssl.SSLError, e:
                    webslinger.LOGGER.log_exception(
                        "Failure in communication from %s. Error message: %s" % (host, str(e)))
                    continue
                except IOError, e:
                    webslinger.LOGGER.log_exception(
                        "Failure in reading or writing from %s. Error message: %s" % (host, str(e)))
                    continue
                with self.lock:
                    self.headers = itertools.chain(self.headers, LockedIterator(itertools.izip(subjects, froms, message_ids, dates, lines, [group] * len(subjects))))
                    self.header_count += len(subjects)
                    group_save = {"name": group, "last": high }
                    db.upsert_group_info(group_save)

            webslinger.LOGGER.log_info("Group %s is up to date. Moving on." % group)
        
        self.connection.quit()

        webslinger.LOGGER.log_info("Finished with downloading headers. Will Recheck in 24 hours.")
        
        
        
class GroupWorker(threading.Thread):

    def __init__(self, conn, keyword):
        threading.Thread.__init__(self)
        self.groups = None
        self.conn = conn
        self.keyword = keyword

    def run(self):
        (resp, groups) = self.conn.list()
        self.conn.quit()
        groups = [(group, int(last) - int(first)) for (group, last, first, flag) in groups if self.keyword in group]
        groups.sort(lambda x, y: y[1] - x[1])
        
        groups = [{"group": group, "size": size}  for (group, size) in groups]
        self.groups = groups





class LockedIterator(object):
    
    def __init__(self, it):
        self.lock = threading.Lock()
        self.it = it.__iter__()

    def __iter__(self): return self

    def next(self):
        self.lock.acquire()
        try:
            return self.it.next()
        finally:
            self.lock.release()


if __name__ == "__main__":
    print("cannot run in a stand alone fashion.")
    
 