# Author: Brett Vann <brett.vann@vannluminous.com>
# URL: http://github.com/roninbv/webslinger/
#
# This file is part of Web Slinger.
#
# Web Slinger is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Web Slinger is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Web Slinger.  If not, see <http://www.gnu.org/licenses/>.

import threading
import webslinger
from datetime import timedelta, date
from threading import Thread
from webslinger.Model.parsers import DiamondParser


class DiamondService(Thread):
    
    def __init__(self, db):
        Thread.__init__(self)
        self.connection = db
        self.active = False
        self.lock = threading.Lock();
        self.parser = DiamondParser()
    
    def run(self):
        
        if self.active == True:
            webslinger.LOGGER.log_info("Diamond Indexer already running. Waiting until the next cycle")
            return 
        
        try:
            self.active = True
            webslinger.LOGGER.log_info( "running diamond job.")
            
            self.connection.clear_diamond_table()
            self.connection.initialize_database(False)
            wednesday_list = self.gen_filenames(date.today())

            for filename in wednesday_list:
                self.parser.set_file_location(filename[0])
                diamond_list = self.parser.get_diamond_list()

                for ele in diamond_list:
                    webslinger.LOGGER.log_info("Parsing %s from diamond" % ele)
                    for elm in diamond_list[ele]:
                        if self.connection.check_diamond_title_exists is False:
                            ish = {'code': elm[0], 'title': elm[1], 'price': elm[2],
                                   'publisher': ele, "publish_date": elm[3].strftime('%m-%d-%Y') }
                            self.connection.add_available_title(ish)
            self.active = False
        except AttributeError as ex:
            self.active = False
            webslinger.LOGGER.log_exception(ex)
        
        webslinger.LOGGER.log_info("List Complete.")

    def is_awake(self):
        webslinger.LOGGER.log_info("active: " + str(self.active))
        return self.active

    def find_wednesday(self, start_day):    
        offset = (start_day.weekday() - 2) % 7
        return start_day - timedelta(days=offset)
    
    
        
    def gen_filenames(self, start_point):
        last_wed = self.find_wednesday(date.today())
        num_weeks = webslinger.DIAMOND_RECORD_WEEKS
        file_names = []
        file_names.append(("http://www.previewsworld.com/shipping/newreleases.txt", last_wed))
        diamond_root = "http://www.previewsworld.com/Archive/GetFile/1/1/71/994/"
        for x in xrange(1, num_weeks):
            last_wed = last_wed - timedelta(days=7)
            file_names.append((diamond_root + "%s.txt" % last_wed.strftime("%m%d%y"), last_wed))
        
        return file_names
        