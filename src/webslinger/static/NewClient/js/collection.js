var Collection = Collection || {};

Collection.seriesPage = function seriesPage(title, pageNum, seriesPages, inCollection, availableIssues) {
    var issues = [];
    return {
        title: title || null,
        pageNum: pageNum || 1,
        seriesPages: seriesPages || 1,
        inCollection: inCollection || 1,
        availableIssues: availableIssues || 1,
        setIssues: function setIssues(issuesIn) {
            issues = issuesIn;
        },

        getissues: function getIssues() {
            return issues;
        },

        addIssue: function addIssue(issue) {
            issues.push(issue);
        },

        render: function () {
            throw new Error("This method is not implemented yet.")
        }
    };
};

Collection.htmlEncode = function (value) {
    'use strict';
    return $('<div/>').text(value).html();
};

Collection.issueProto = {
    id: null,
    comicVineId: null,
    title: '',
    number: '',
    type: 'ONGOING',
    iconUrl: null,
    fileUrl: null,
    read: false,
    render: function () {
        throw new Error("Not implemented.");
    }
};

Collection.comicIssue = function comicIssue(ish) {
    return $.extend(true, Object.create(Collection.issueProto), ish);
};


Collection.menuProto = {
    series: [],
    render: function (containerId) {
        var menu = $("#" + containerId);
        menu.html("");
        for (var i = 0; i < this.series.length; i++) {
            menu.append('<button id="' + this.series[i].id + '" onclick="Collection.seriesMenu.loadSeriesPage(' + i + ')" class="btn btn-info" >' + this.series[i].label + '</button');
        }
    },
    loadSeriesPage: function (seriesIndex, page, issueType) {
        Collection.currentSeries = this.series[seriesIndex];
        $.getJSON('/collection/issues', {
            series: Collection.currentSeries.id,
            page: page,
            issue_type: issueType
        }, function (issues) {
            Collection.currentPage = Collection.seriesPage(Collection.currentSeries.title, issues.page, issues.total_pages, issues.in_collection, issues.records);
            for (var i = 0; i < issues.rows.length; i++) {
                var ish = Collection.comicIssue();
                ish.id = issues.rows[i].id;
                ish.title = issues.rows[i].title;
                ish.number = issues.rows[i].issueNumber;
                ish.type = issues.rows[i].issue_type;
                ish.read = issues.rows[i].isRead == 1;
                ish.comicVineId = issues.rows[i].comic_vine_id;
                ish.iconUrl = issues.rows[i].icon_url;
                ish.fileUrl = issues.rows[i].url;
                Collection.currentPage.addIssue(ish);
            }

            console.log(Collection.currentPage);
        });
    },
    refresh: function (publishers) {
        var params = {
            types: {
                mini: $("#chxMini").prop('checked'),
                one_shot: $("#chxOneShot").prop('checked'),
                regular: $("#chxOngoing").prop('checked'),
            },
            publishers: [],
            status:{
                current: $("#chxCurrent").prop('checked'),
                achived: $("#chxArchived").prop('checked')
            }
        };

        $(publishers).each(function(ind, elem){
           params.publishers.push({publisher_id:  elem.id, active: $("#chxPublisher_" + elem.id).prop('checked')});
        });

        console.log(params);

        $.ajax({
            type: "POST",
            url: "/collection/menu",
            data: JSON.stringify(params),
            contentType: 'application/json',
            dataType: 'json',
            error: function() {
                alert("error");
            },
            success: function(series) {
                Collection.menuProto.series = series;
                Collection.menuProto.render('series-menu-container');
            }
        });
    }
};


Collection.viewFilterProto = {
    filteredItems: [],
    publishers: [],
    refresh: function () {
        $(Collection.viewFilterProto.filteredItems).each(function (ind, elem) {
            elem.refresh(Collection.viewFilterProto.publishers);
        });
    },
    initFilterCheckType: function (type) {
        var className = '.chx' + type,
            idName = '#chxAll' + type;

        $(className).prop('checked', $(idName).prop('checked'));

        $(idName).click(function () {
            $(className).prop('checked', $(this).prop('checked'));
        });
        $(className).click(function () {
            var allchecked = true;
            $(className).each(function (index, elm) {
                if (!$(elm).prop('checked')) {
                    allchecked = false;
                }
            });
            $(idName).prop('checked', allchecked);
        });
    },
    render: function () {
        var publishers = $("#contPublishers");
        publishers.append('<div class="col-md-2"> <input type="checkbox" id="chxAllPublishers" checked="checked"/><label for="chxAllPublishers">All</label></div>');

        for (var i = 0; i < this.publishers.length; i++) {
            publishers.append('<div class="col-md-2"><input type="checkbox" class="chxPublishers" id="chxPublisher_'+ this.publishers[i].id + '" checked="checked"/><label for="chx'+ this.publishers[i].name + '">'+ this.publishers[i].name + '</label></div>');
        }
    },
    init: function () {

        $.getJSON('/collection/publishers', null, function (publishers) {

            Collection.viewFilterProto.publishers = publishers;

            Collection.viewFilterProto.render();
            Collection.viewFilterProto.initFilterCheckType("Types");
            Collection.viewFilterProto.initFilterCheckType("Publishers");
            Collection.viewFilterProto.initFilterCheckType("Status");
            $("input[type=checkbox]").click(Collection.viewFilterProto.refresh);
            Collection.viewFilterProto.refresh();
        });
    }
};

Collection.init = function () {
    //get menu.
    var seriesMenu = Object.create(Collection.menuProto);
    var filter = $.extend(true, Object.create(Collection.viewFilterProto), {filteredItems: [seriesMenu]});
    filter.init();
};
