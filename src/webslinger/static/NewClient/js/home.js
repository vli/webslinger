var News = News || {};


function NewsViewModel() {
    'use strict';

    this.init = function () {

        var js = null,
            fjs = document.getElementsByTagName("script")[0];

        if (!document.getElementById("twitter-wjs")) {
            js = document.createElement("script");
            js.id = "twitter.wjs";
            js.src = "//platform.twitter.com/widgets.js";
            fjs.parentNode.insertBefore(js, fjs);
        }

        $("#cbrnews").FeedEk({
            FeedUrl: "http://www.comicbookresources.com/feed.php?feed=news"
        });
        $("#pwupcoming").FeedEk({
            FeedUrl: "http://www.previewsworld.com/Rss/1/1/71"
        });
    };
}


$(function () {
    'use strict';
    News.viewModel = new NewsViewModel();
    News.viewModel.init();
});