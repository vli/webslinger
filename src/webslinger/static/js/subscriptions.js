var Subs = Subs || {};

function DigitallyAvailableViewModel(apiKey) {
    'use strict';
    var self = this;
    this.apiKey = apiKey;
    this.parent = null;
    this.numQueryResults = ko.observable(0);
    this.pager = ko.observableArray([]);
    this.titles = ko.observableArray([]);
    this.currentIndex = 1;


    this.init = function (elementId, parent) {
        self.parent = parent;
        ko.applyBindings(self, document.getElementById(elementId));
        $("#txtQuery").keypress(function (event) {

            if (event.which === 13) {
                self.searchTitles();
            }
        });
    };

    this.searchTitles = function () {
        self.titles.removeAll();

        var searchTerm = $("#txtQuery").val(),
            params = {
                api_key: self.apiKey,
                filter: "name:" + searchTerm,
                sort: "name:" + searchTerm,
                limit: 10,
                offset: (self.currentIndex - 1) * 10,
                field_list: "name,start_year,count_of_issues,image,id,publisher,description",
                resources: "volume"
            },
            query = "http://www.comicvine.com/api/volumes/?format=jsonp&json_callback=?",
            pages = null,
            i = 1;


        $.get(query, params, function (data) {
            self.pager.removeAll();
            pages = data.number_of_page_results;

            for (i = 1; i <= pages; i += 1) {
                if (i === self.currentIndex) {
                    self.pager.push({text: i, page: i, selected: true});
                } else {
                    self.pager.push({text: i, page: i, selected: false});
                }
            }

            self.numQueryResults(data.number_of_total_results);

            for (i = 0; i < data.results.length; i += 1) {
                if (!data.results[i].publisher) {
                    data.results[i].publisher = {name: "?"};
                }

                if (!data.results[i].image) {
                    data.results[i].image = {screen_url: "/static/images/imageNotAvailable.jpg", icon_url: ""};
                }
            }

            self.titles(data.results);
            self.wireUpPagerEffects();
        }, 'json');
    };

    this.showCover = function (data, event) {

        $("#dialog-container").html("");
        var html = "<img src='" + data.image.super_url + "' style='width:400px;'/><div style='height:200px;overflow-y:auto; margin:5px;'>" + data.description + "</div>";
        $("#dialog-container").html(html);
        $("#dialog-container").dialog({
            modal: true,
            position: 'top',
            width: 430,
            resizable: false,
            draggable: false
        });
    };

    this.addTitleSub = function (data, event) {
        var url = "/subscription/add_title",
            parms = {
                title: data.name,
                id: data.id,
                publisher: data.publisher.name,
                image_url: data.image.screen_url
            };

        $.get(url, parms, function (data) {
            if (data.success) {
                $("#alert-text").text("The title was added to your digital pull list.");
                $("#alert-container").addClass("alert-success");
                $("#alert-container").show('blind', {direction: 'vertical'});
                self.parent.refreshPullList();
            }
        }, 'json');
    };

    this.pageTitles = function (data, event) {
        self.currentIndex = data.page;
        console.log(data);
        self.searchTitles();
    };

    this.wireUpPagerEffects = function () {
        $('.pager-button').hover(function () {
            $(this).addClass("ui-state-hover");
        }, function () {
            $(this).removeClass("ui-state-hover");
        });
    };
}


function CurrentPullListViewModel() {
    'use strict';
    var self = this;
    this.currentIndex = 1;
    this.parent = null;
    this.subscribedTitles = ko.observableArray([]);

    this.init = function (elementId, parent) {
        self.parent = parent;
        self.getData(1);
        ko.applyBindings(self, document.getElementById(elementId));
        self.parent.initButtons();
    };

    this.getData = function (pageIndex) {
        var url = "/subscription/get_digital_pull",
            params = {
                index: pageIndex
            };

        $.get(url, params, function (data) {
            self.subscribedTitles(data);
        }, 'json');
    };

    this.deleteTitle = function (data, event) {
        var url = "/subscription/delete_title",
            param = {
                id: data.id
            };

        $.post(url, param, function (data) {
            self.refresh();
        });
    };

    this.refresh = function () {
        self.getData(self.currentIndex);
    };
}

function DigitalSubscriptionViewModel(api) {
    'use strict';
    var self = this;
    this.available = new DigitallyAvailableViewModel(api);
    this.pulllist = new CurrentPullListViewModel();

    this.init = function () {
        self.available.init("titleSearchView", self);
        self.pulllist.init("currentPullView", self);
    };

    this.refreshPullList = function () {
        self.pulllist.refresh();
    };

    this.refreshSearchview = function () {
        self.available.refresh();
    };

    this.initButtons = function () {
        $(".ui-state-default").hover(function () {
            $(this).addClass("ui-state-hover");
        }, function () {
            $(this).removeClass("ui-state-hover");
        });
    };
}



