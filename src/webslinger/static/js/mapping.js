var Subs = Subs || {};

function DigitallyAvailableViewModel(apiKey) {
    'use strict';
    var self = this;
    this.apiKey = apiKey;
    this.parent = parent;
    this.numQueryResults = ko.observable(0);
    this.pager = ko.observableArray([]);
    this.titles = ko.observableArray([]);
    this.currentIndex = 1;


    this.init = function (elementId) {
        ko.applyBindings(self, document.getElementById(elementId));

        $("#txtQuery").keypress(function (event) {
            if (event.which === 13) {
                self.searchTitles();
            }
        });
    };

    this.quickSearch = function (term) {
        $("#txtQuery").val(term);
        self.searchTitles();
    };

    this.searchTitles = function (offset) {
        self.titles.removeAll();
        offset = offset || 0;

        var searchTerm = $("#txtQuery").val(),
            params = {
                api_key: self.apiKey,
                limit: 10,
                offset: (offset * 10),
                field_list: "name,start_year,count_of_issues,image,id,publisher,description",
                filter: "name:" + searchTerm
            },
            i = 0,
            query = "http://comicvine.com/api/volumes/?format=jsonp&json_callback=?";

        $.get(query, params, function (data) {
            self.pager.removeAll();

            var pages = data.number_of_total_results / 10;

            for (i = 0; i < pages; i += 1) {
                if (i === offset) {
                    self.pager.push({text: i + 1, page: i, selected: true});
                } else {
                    self.pager.push({text: i + 1, page: i, selected: false});
                }
            }
            self.numQueryResults(data.number_of_total_results);

            for (i = 0; i < data.results.length; i += 1) {
                if (!data.results[i].publisher) {
                    data.results[i].publisher = {name: "?"};
                }

                if (!data.results[i].image) {
                    data.results[i].image = {screen_url: "/static/images/imageNotAvailable.jpg", icon_url: ""};
                }
            }

            self.titles(data.results);
            self.wireUpPagerEffects(offset);
        }, 'json');
    };

    this.addTitleSub = function (data, event) {
        var url = "/collection/map_series",
            new_cv = data.id,

            title = {
                series_id: Collection.viewModel.currentSeries,
                cv_id: data.id,
                publisher: data.publisher.name,
                image_url: data.image.screen_url
            },
            parms = {
                vm: ko.toJSON(title)
            };

        $.get(url, parms, function (data) {
            if (data.success) {
                $("#dialog-container").dialog("close");
                alert("The series was successfully mapped.");
                console.log(self.parent);
                self.parent.currentCvId(new_cv);
                self.parent.currentCvId.valueHasMutated();
            } else {
                alert("There was some sort of error. Please try again.");
            }

        }, 'json');
    };

    this.pageTitles = function (data, event) {
        self.currentIndex = data.page;
        self.searchTitles(data.page);
    };

    this.setParent = function (parent) {
        self.parent = parent;
    };

    this.wireUpPagerEffects = function (index) {
        $('.pager-button').hover(function () {
            $(this).addClass("ui-state-hover");
        }, function () {
            $(this).removeClass("ui-state-hover");
        });
    };
}


