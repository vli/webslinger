function LatestIssuesReport(){
	var self = this;
	this.issues = ko.observableArray([]);
	this.latestCount = ko.observable(null);
	
	this.init = function(){
		$.getJSON('/reports/latest', null, function(retdata){
			self.issues(retdata.issues);
			self.latestCount(retdata.count);
		});
		
		ko.applyBindings(self, document.getElementById('latest-issues-container'));
	}
}