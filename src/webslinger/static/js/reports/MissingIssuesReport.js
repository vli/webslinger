

function MissingIssuesReport(){
	var self = this;
	this.issues = ko.observableArray([]);
	this.missingCount = ko.observable(null);
	
	this.init = function(){
		$.getJSON('/reports/missing', null, function(retdata){
			self.issues(retdata.issues);
			self.missingCount(retdata.count);
		});
		
		ko.applyBindings(self, document.getElementById('missing-issues-container'));
	}
}