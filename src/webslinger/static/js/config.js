function GeneralSettingsVm() {
	var self = this;
	this.comicVineKey = ko.observable("");
	this.collectionPath = ko.observable("");
	this.rescanFrequency = ko.observable("");
	this.rescanDenomination = ko.observable("");
	this.serviceActive = ko.observable(false);
	this.currentDir = ko.observable("");

	this.init = function(elementId) {
		url = "/config/get_gen_settings";

		ko.applyBindings(self, document.getElementById(elementId));

		$.get(url, null, function(data) {
			self.comicVineKey(data.comic_vine_api);
			self.collectionPath(data.path);
			self.rescanFrequency(data.rescan_freq);
			self.rescanDenomination(data.rescan_denom);
			self.serviceActive(data.service_active);
			self.currentDir(data.cwd);

		}, 'json');
	};

	this.saveSettings = function() {
		var params = {
			settings : ko.toJSON(self)
		};
		$.post("/config/save_gen_settings", params, function(data) {
			//TODO: Show a dialog here. confirming that the update happened.
		}, 'json');

	};
}

function DatabaseSettingsVm() {
	var self = this;
	this.dataStore = ko.observable(1);
	this.dbHost = ko.observable("webslinger.sqlite");
	this.dbPort = ko.observable("");
	this.dbUser = ko.observable("");
	this.dbPass = ko.observable("");
	this.dbName = ko.observable("");
	this.hostType = ko.observable("File:");

	this.mongoDefaults = {
		host : "localhost",
		port : 27017,
		name : "webslinger_db",
	};

	this.mysqlDefaults = {
		host : "localhost",
		port : 3306,
		name : "webslinger_db",
	};

	this.sqliteDefaults = {
		host : "webslinger.sqlite"
	};

	this.init = function(elementId) {

		var url = "/config/get_db_settings";

		$.get(url, null, function(data) {
			self.dataStore(data.db_store);
			self.dbHost(data.db_host);
			self.dbPort(data.db_port);
			self.dbName(data.db_name);
			self.dbUser(data.db_user);
			self.dbPass(data.db_pass);
			self.initSubscriptions();
		}, 'json');

		ko.applyBindings(this, document.getElementById(elementId));
	};

	this.initSubscriptions = function() {
		self.dataStore.subscribe(function(newVal) {
			switch(newVal) {
				case "1":{
						self.dbHost(self.sqliteDefaults.host);
						self.dbPort(0);
						self.dbName("");
						self.dbUser("");
						self.dbPass("");
						self.hostType("File:");
					}
					break;
				case "2":{
					self.dbHost(self.mysqlDefaults.host);
					self.dbPort(self.mysqlDefaults.port);
					self.dbName(self.mysqlDefaults.name);
					self.dbUser("");
					self.dbPass("");
					self.hostType("Host:");
				}
			}
		});
	};

	this.clearDatabase = function() {
		var message = "This will delete all data in the database. \n Are you sure?";
		if (confirm(message)) {
			//Delete that shizzz.
			$.post("/config/init_database", {
				sure : true
			}, function(data) {
				alert("The database has been initialized.");
			})
		}
	};

	this.saveSettings = function() {
		var url = "/config/save_db_settings";
		var param = {
			settings : ko.toJSON(self)
		}

		$.post(url, param, function(data) {
			alert(JSON.stringify(data));
		});
	};
}

function UsenetVM() {
	var self = this;
	this.groups = ko.observableArray([]);
	this.selectedGroups = ko.observableArray([]);

	this.init = function(elementId) {
		self.getCurrentGroups();
		ko.applyBindings(this, document.getElementById(elementId));
	};

	this.checkButtonState = function(data, e) {
		if ($(e.target).is(":checked")) {
			self.selectedGroups.push(data);
			return true;
		} else {
			self.selectedGroups.pop(data);
			return true;
		}
	};

	this.getCurrentGroups = function() {
		$.get('/config/get_groups', null, function(data) {
			self.selectedGroups(data);
		}, 'json');
	};

	this.saveSelectedGroupList = function() {
		var param = {
			group_list : ko.toJSON(self.selectedGroups)
		};

		$.post("/config/save_groups", param, function(data) {
			alert(JSON.stringify(data))
		});
	};

	this.removeGroup = function(data, e) {
		self.selectedGroups.pop(data);
	};

	this.groupsToSave = ko.computed(function() {

		if (self.selectedGroups().length < 1) {
			$("#addButton").addClass("ui-state-disabled");
			return true;
		} else {
			$("#addButton").removeClass("ui-state-disabled");
			return false;
		}

	}, this);

	this.getGroups = function() {
		$('#loadButton').attr('disabled', 'disabled');

		function poll() {
			$.ajax({
				url : '/config/list_groups',
				data : null,
				type : 'post',
				datatype : 'json',
				beforeSend : function() {
					$('#loadButton').attr('disabled', 'disabled');
					$('#error').empty();
				},
				success : function(data) {
					if (data['status'] == "working") {
						window.setTimeout(poll, 2000);
					} else if (data['status'] == "error") {
						$('#error').empty().append(data['result']);
						$('#loadButton').removeAttr('disabled');
					} else {
						self.groups(data.result);
						$('#load_button').removeAttr('disabled');
					}
				},
				error : function(jqXHR, textStatus, errorThrown) {
					$('#error').empty().append(errorThrown);
					$('#load_button').removeAttr('disabled');
				},
				complete : function() {
				}
			});
		}
		poll();
	};
}

var Config = Config || {};
