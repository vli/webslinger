var Collection = Collection || {},
    Utils = Utils || {};

Utils.stringBuilder = function (value) {
    this.strings = [''];
    this.append = function (value) {
        if (value) {
            this.strings.push(value);
        }
    };

    this.clear= function () {
        this.strings = [];
    };

    this.toString = function () {
        return this.strings.join("");
    };
    if(value){
        this.strings = [value];
    }
};

Collection.htmlEncode = function (value) {
    'use strict';
    return $('<div/>').text(value).html();
};

Collection.TabVM = function (parent, elementId, issueType) {
    'use strict';
    var self = this;
    this.parent = parent;
    this.issues = ko.observableArray([]);
    this.pager = ko.observableArray([]);
    this.page = ko.observable(1);
    this.issueCount = ko.observable(0);
    this.inCollection = ko.observable(0);
    this.elementId = elementId;
    this.issueType = issueType;

    this.init = function () {
        self.getIssues();
        ko.applyBindings(self, document.getElementById(self.elementId));
    };

    this.displaySummary = function (data, e) {
        self.parent.displaySummary(data, e);
    };

    this.toggleRead = function (data, e) {
        self.parent.toggleRead(data, e);
        return true;
    };

    this.getIssues = function () {
        var param = {
                series: self.parent.currentSeries(),
                page: self.page()
            },
            i = 1;

        if (self.issueType && self.issueType !== 1) {
            param.issue_type = self.issueType;
        }

        $.get("/collection/issues", param, function (ret) {
            self.pager.removeAll();

            for (i = 1; i <= ret.total_pages; i += 1) {
                self.pager.push({
                    label: i + String()
                });
            }

            self.page(ret.page);
            self.issues.removeAll();
            self.issues(ret.rows);
            self.issueCount(ret.records);
            self.inCollection(ret.in_collection);
            self.parent.initButtonEffects();
            self.page.valueHasMutated();
        }, 'json');
    };

    this.selectPage = function (data) {
        self.page(data.label);
        self.page.valueHasMutated();
        self.getIssues();
    };

    this.downloadFile = function (data) {
        var filepath = Collection.htmlEncode(data.url);
        window.open("download?filepath=" + filepath);
    };

    this.page.subscribe(function (newValue) {
        switch (self.issueType) {
        case Collection.IssueTypes.ANNUAL:
            $('.annual-pager-button').removeClass("ui-state-active");
            $("#annual_pager_" + newValue).addClass("ui-state-active");
            break;
        case Collection.IssueTypes.KING_SIZE:
            $('.king-pager-button').removeClass("ui-state-active");
            $("#king_pager_" + newValue).addClass("ui-state-active");
            break;
        case Collection.IssueTypes.SPECIAL:
            $('.special-pager-button').removeClass("ui-state-active");
            $("#special_pager_" + newValue).addClass("ui-state-active");
            break;
        default:
            $('.issue-pager-button').removeClass("ui-state-active");
            $("#issue_pager_" + newValue).addClass("ui-state-active");
        }
    });
}


Collection.InfoTabVM = function(parent, apiKey) {
    'use strict';
    var self = this;
    this.isMapped = ko.observable(false);
    this.seriestext = ko.observable(null);
    this.imageurl = ko.observable(null);
    this.parent = parent;

    this.init = function () {
        ko.applyBindings(self, document.getElementById("infoView"));

        self.getIsMapped(parent.currentSeries());

        parent.currentSeries.subscribe(function (newVal) {
            self.getIsMapped(newVal);
        });
    };

    parent.currentCvId.subscribe(function (newVal) {
        if (newVal) {
            var url = "http://api.comicvine.com/volume/4050-" + newVal + "/?format=jsonp&json_callback=?",
                param = {
                    api_key: apiKey
                };

            $.get(url, param, function (data) {
                self.isMapped(true);
                self.seriestext(data.results.description);
                self.imageurl(data.results.image.super_url);
                self.seriestext.valueHasMutated();
                //self.imageurl.valueHasMutated();
                //self.isMapped.valueHasMutated();
            }, 'json');
        }
    });

    this.getIsMapped = function (newVal) {
        var param = {
            Id: newVal
        };
        $.get('/collection/is_in_pull', param, function (data) {
            console.info(JSON.stringify(data));
            if (data.in_pull instanceof Array) {
                data.in_pull = data.in_pull[0];
            }

            self.isMapped(data.in_pull);
        }, 'json');
    };

    this.showEditMetadata = function () {
        alert("Not Implemented.");
    };

    this.showDialog = function () {
        $("#dialog-content").load("/collection/mappingdialog", null, function () {
            $("#dialog-container").dialog({modal: true, width: 1000, position: 'top'}).show();
            Subs.viewModel.setParent(self.parent);
            Subs.viewModel.quickSearch(self.parent.seriesTitle());
        });
    };
}

Collection.CollectionPage = function (apiKey) {
    'use strict';
    var self = this;
    this.menuitem = ko.observableArray([]);
    this.currentSeries = ko.observable(null);
    this.currentCvId = ko.observable(null);
    this.issues = new Collection.TabVM(this, "issueView", Collection.IssueTypes.NORMAL);
    this.annuals = new Collection.TabVM(this, "annualView", Collection.IssueTypes.ANNUAL);
    this.kings = new Collection.TabVM(this, "kingView", Collection.IssueTypes.KING_SIZE);
    this.specials = new Collection.TabVM(this, "specialView", Collection.IssueTypes.SPECIAL);
    this.seriesInfo = new Collection.InfoTabVM(this, apiKey);
    this.seriesTitle = ko.observable("");
    this.displayTitle = ko.observable("");
    this.showMini = ko.observable(false);
    this.showOneShot = ko.observable(false);
    this.showRegular = ko.observable(true);
    this.apiKey = apiKey;

    function openDialog(imageUrl, comboTitle, description) {
        var html = new Utils.stringBuilder();
        html.append("<div >");
        html.append(imageUrl);
        html.append("</div><div style='height:250px;overflow:auto;padding:2px;'><h3>");
        html.append(comboTitle);
        html.append("</h3>");
        html.append(description);
        html.append("</div>");
        $("#dialog-content").html(html.toString());
        $('#dialog-container').dialog({
            modal: true,
            resizable: false,
            title: 'Detail',
            width: 650,
            position: "top",
            close: function () {
                $(this).html('');
                $(this).dialog('close');
            }
        });
    }

    function init() {
        $.get("/collection/menu", {show_mini: self.showMini(), show_regular: self.showRegular(), show_one_shot: self.showOneShot()}, function (data) {
            self.menuitem(data);
            if (self.menuitem().length === 0) {
                self.currentSeries(0);
            } else {
                self.currentSeries(Collection.htmlEncode(self.menuitem()[0].id));
                self.seriesTitle(self.menuitem()[0].title);
                self.currentCvId(self.menuitem()[0].cv_id);
                self.displayTitle(self.menuitem()[0].label);
            }

            self.issues.init();
            self.annuals.init();
            self.kings.init();
            self.specials.init();
            self.seriesInfo.init();
            ko.applyBindings(self, document.getElementById("pageView"));

            self.showMini.subscribe(self.updateSeriesTypeFilter);
            self.showRegular.subscribe(self.updateSeriesTypeFilter);
            self.showOneShot.subscribe(self.updateSeriesTypeFilter);

        }, 'json');
    }

    this.updateSeriesTypeFilter = function(val) {
        $.get("/collection/menu", {show_mini: self.showMini(), show_regular: self.showRegular(), show_one_shot: self.showOneShot()},
            function (data) {
                self.menuitem(data);
                $("#menu_item_" + self.currentSeries()).addClass("ui-state-active");
            }, 'json');
    };

    this.toggleRead = function (data) {
        var url = "/collection/update_read",
            param = {
                is_read: data.isRead,
                issue_id: data.id
            };

        $.get(url, param, function (ret) {
            data.isRead = ret.read;
        }, 'json');
        return true;
    };

    this.initButtonEffects = function () {
        $(".ui-state-default").hover(function () {
            $(this).addClass("ui-state-hover");
        }, function () {
            $(this).removeClass("ui-state-hover");
        });
    };

    this.displaySummary = function (koObj) {
        var comboTitle = (koObj.title) ? koObj.title + " " + koObj.issueNumber : "Issue #" + koObj.issueNumber,
            param = {
                api_key: self.apiKey
            },
            imageUrl = "<img src='/collection/image?Id=" + koObj.id + "' style='margin:6px;width:600px; ' />",
            description = "No summary found... have you mapped this series yet?",
            url = null;


        if (koObj.comic_vine_id) {
            url = "http://api.comicvine.com/issue/" + koObj.comic_vine_id + "/?format=jsonp&json_callback=?";
            $.get(url, param, function (data) {
                if (data.results.image) {
                    imageUrl = "<img src='" + data.results.image.super_url + "' style='margin:6px;width:600px; ' />";
                }
                description = data.results.description || description;
                openDialog(imageUrl, comboTitle, description);
            }, 'json');
        } else {
            openDialog(imageUrl, comboTitle, description);
        }
    };

    this.menuClick = function (data) {
        $(".collection-menu-button").removeClass("ui-state-active");
        self.currentCvId(data.cv_id);
        self.currentSeries(data.id);
        self.seriesTitle(data.title);
        self.displayTitle(data.label);
        self.issues.selectPage({
            label: 1
        });
        self.annuals.selectPage({
            label: 1
        });
        self.kings.selectPage({label: 1});
        self.specials.selectPage({label: 1});
        $("#menu_item_" + self.currentSeries()).addClass("ui-state-active");
    };

    init();
};



