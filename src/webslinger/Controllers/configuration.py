# Author: Brett Vann <brett.vann@vannluminous.com>
# URL: http://github.com/roninbv/webslinger/
#
# This file is part of Web Slinger.
#
# Web Slinger is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Web Slinger is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Web Slinger.  If not, see <http://www.gnu.org/licenses/>.


import cherrypy
import json
import os
import webslinger
from ConfigParser import SafeConfigParser
from mako.lookup import TemplateLookup
from webslinger.Model.entities import GeneralSettings, DatabaseSettings
from webslinger.Model.parsers import NewsGroupHeaderParser
from webslinger.Services.nntp_indexer import GroupWorker, ArticleGrabber


class ConfigurationController:
    
    def __init__(self,db):
        self.db = db
        self._workers = {}
        self.lookup = TemplateLookup(["webslinger/Views","webslinger/Views/config"])
        self.configParser = SafeConfigParser();
        self.configParser.read('webslinger.ini')
    
    @cherrypy.expose
    def index(self):
        return self.lookup.get_template("index.html").render(title = "General Configuration")
    
    @cherrypy.expose
    def database(self):
        return self.lookup.get_template("database.html").render(title = "Database Configuration")
    
    @cherrypy.expose
    def servers(self):
        return self.lookup.get_template("usenet.html").render(title = "Usenet Configuration")
    
    @cherrypy.expose
    def get_gen_settings(self):
        
        configSettings = GeneralSettings()
        #configSettings.comic_vine_api = self.configParser.get("comicvine", "api_key")
        configSettings.path = self.configParser.get("digital_collection", "path") 
        configSettings.rescan_freq = self.configParser.getint("digital_collection", "rescan_freq") 
        configSettings.rescan_denom = self.configParser.getint("digital_collection", "rescan_denom")
        configSettings.service_active = self.configParser.getboolean("digital_collection", "service_active") 
        configSettings.cwd = os.getcwd()
        return json.dumps(configSettings.__dict__)

    @cherrypy.expose
    def save_gen_settings(self, settings):
        
        settings_py = json.loads(settings)
        #config.set("comicvine", "api_key", str(settings_py["comicVineKey"] ))
        self.configParser.set("digital_collection", "path",  str(settings_py["collectionPath"]))
        self.configParser.set("digital_collection", "rescan_freq", settings_py["rescanFrequency"])
        self.configParser.set("digital_collection", "rescan_denom", settings_py["rescanDenomination"])
        self.configParser.set("digital_collection", "service_active", str(settings_py["serviceActive"]))
        
        with open('webslinger.ini', 'wb') as configfile:
            self.configParser.write(configfile)
        
        return json.dumps(settings_py);
    
    @cherrypy.expose
    def init_database(self, sure):
        if sure == 'true':
            self.db.initialize_database()
        return json.dumps({"success": True});

    @cherrypy.expose
    def get_db_settings(self):
        self.configParser.read('webslinger.ini')
        configSettings = DatabaseSettings()
        configSettings.db_store = self.configParser.getint("database", "data_store")
        configSettings.db_host = self.configParser.get("database", "host").strip('"') 
        configSettings.db_port = self.configParser.getint("database", "port")
        configSettings.db_name = self.configParser.get("database", "name").strip('"') 
        configSettings.db_user = self.configParser.get("database", "db_user").strip('"') 
        configSettings.db_pass = self.configParser.get("database", "db_pass").strip('"') 
        return json.dumps(configSettings.__dict__)
    
    @cherrypy.expose
    def save_db_settings(self, settings):
        settings_py = json.loads(settings)
        self.configParser.set("database", "data_store", str(settings_py["dataStore"]))
        self.configParser.set("database", "host", '"' + settings_py["dbHost"] + '"') 
        self.configParser.set("database", "port", str(settings_py["dbPort"]))
        self.configParser.set("database", "name", '"' + settings_py["dbName"] + '"') 
        self.configParser.set("database", "db_user", '"' + settings_py["dbUser"] + '"') 
        self.configParser.set("database", "db_pass", '"' +  settings_py["dbPass"] + '"')
        
        with open('webslinger.ini', 'wb') as configfile:
            self.configParser.write(configfile)
        
        return json.dumps(settings_py)
    
    @cherrypy.expose
    def list_groups(self ):
        uid = (webslinger.NNTP_SERVER, webslinger.NNTP_PORT, webslinger.NNTP_USE_SSL, webslinger.NNTP_USER, webslinger.NNTP_PASSWORD, "comics")
        cherrypy.response.headers['Content-type'] = 'application/json'

        try:
            worker = self._workers[uid]
            if worker.groups is not None:
                if len(worker.groups) > 0:
                    return json.dumps({'status' : 'done', 'result' : worker.groups})
                else:
                    return json.dumps({'status' : 'error', 'result' : 'No Newsgroups Found.'})

            return json.dumps({'status' : 'working', 'result' : '' })

        except KeyError, e:
            try:
                port = int(uid[1])
            except ValueError, e:
                return json.dumps({'status' : 'error', 'result' : "Settings Error: Invalid Port."})

            try:
                conn = ArticleGrabber().connect(host=uid[0], port=port, is_ssl=uid[2], username=uid[3], password=uid[4], retry=1)
            except IOError, e:
                return json.dumps({'status' : 'error', 'result' : 'Settings Error: %s' % str(e), 'settings': uid})

            self._workers[uid] = GroupWorker(conn, uid[5])
            self._workers[uid].start()
            return json.dumps({'status' : 'working', 'result' : '' })
        
        
    @cherrypy.expose
    def save_groups(self, group_list):
        cherrypy.response.headers['Content-type'] = 'application/json'
        groups = json.loads(group_list);
        for group in groups:
            self.db.add_group(group)
        return json.dumps(groups);
    

    
    
    @cherrypy.expose
    def get_groups(self):
        return json.dumps(self.db.get_groups());
    
    @cherrypy.expose()
    def execute_query(self, title, number = None, volume = None):
        
        if number is not None:
            title = "%s %%%s" % (title, number)
        
        sql = "SELECT subject, date_posted, filename FROM article where subject LIKE '%%%s%%'" % title
        db_result = webslinger.ARTICLE_CONNECTION.select(sql )
        results = []
        header_parser = NewsGroupHeaderParser()
        for result in db_result:
            pamend = {"date_posted": result[1],"filename": result[2], "parse_result": header_parser.parse(result[0])}
            results.append(pamend)
        return json.dumps({"query": sql, "results":results})
    


if __name__ == "__main__":
    print "Cannot be run in a standalone fashion."
