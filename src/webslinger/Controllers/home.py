# Author: Brett Vann <brett.vann@vannluminous.com>
# URL: http://github.com/roninbv/webslinger/
#
# This file is part of Web Slinger.
#
# Web Slinger is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Web Slinger is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Web Slinger.  If not, see <http://www.gnu.org/licenses/>.

import cherrypy
from mako.lookup import TemplateLookup
from webslinger.Controllers.collection import CollectionController
from webslinger.Controllers.configuration import ConfigurationController
from webslinger.Controllers.subscriptions import SubscriptionController
from webslinger.Controllers.report import ReportController

class HomePageController:
    

    def __init__(self, data_access):
        self.db = data_access
        self.lookup = TemplateLookup(directories=["webslinger/Views","webslinger/Views/home"])
        self.collection = CollectionController(self.db)
        self.subscription = SubscriptionController(self.db)
        self.config = ConfigurationController(self.db)
        self.reports = ReportController(self.db)
        
    @cherrypy.expose
    def index(self):
        return self.lookup.get_template("index.html").render(title="Home")
    
    
    
    

            


    


