import cherrypy
import json
import re
import webslinger
from mako.lookup import TemplateLookup


class ReportController(object):

    def __init__(self, db):
            self.db = db
            self.lookup = TemplateLookup(directories=["webslinger/Views","webslinger/Views/reports"])
        
    @cherrypy.expose
    def missing(self):
        result = self.db.get_missing_issues()
        return json.dumps({"count": len(result), "issues": result})
        
    @cherrypy.expose
    def index(self):
        return self.lookup.get_template("index.html").render(title="Reporting")
    
    @cherrypy.expose
    def missingIssues(self):
        return self.lookup.get_template("missing.html").render(title="Reporting")
    
    @cherrypy.expose
    def latest(self):
        result = self.db.get_latest_issues()
        return json.dumps({"count": len(result), "issues": result})
    
    @cherrypy.expose
    def latestIssues(self):
        return self.lookup.get_template("latest.html").render(title="Reporting")