# Author: Brett Vann <brett.vann@vannluminous.com>
# URL: http://github.com/roninbv/webslinger/
#
# This file is part of Web Slinger.
#
# Web Slinger is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Web Slinger is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Web Slinger.  If not, see <http://www.gnu.org/licenses/>.

import cherrypy
import re
import json
import webslinger
from mako.lookup import TemplateLookup
from webslinger.Model.entities import ComicSeries


class SubscriptionController:
    def __init__(self,db):
        self.db = db
        self.lookup = TemplateLookup(["webslinger/Views","webslinger/Views/pulllist"])
        
    @cherrypy.expose
    def digital(self):
        return self.lookup.get_template("digital.html").render(api = webslinger.COMIC_VINE_API_KEY, title="Digital Pull List")
    
    @cherrypy.expose
    def printed(self):
        return self.lookup.get_template("printed.html").render(api = webslinger.COMIC_VINE_API_KEY, title="Printed Pull List")
    
    @cherrypy.expose
    def add_title(self, id, title, publisher, image_url):
        title = ComicSeries(None, title, False, publisher = publisher, image =  image_url, comic_vine_id = id)
        self.db.add_pull_list_title(title);
        return json.dumps({"success": True})
   
    @cherrypy.expose 
    def delete_title(self, id):
        self.db.delete_pull_title(id);
        return json.dumps({"success": True})
    
    @cherrypy.expose
    def get_digital_pull(self, index = 1, rows = 10):
        index = int(index)
        cherrypy.response.headers["Content-Type"] = "application/json"
        return json.dumps(self.db.get_current_pull_list(index, rows))
    
    @cherrypy.expose
    def detail(self, _id):
        issues = self.db.get_pull_list_issues(_id)
        return self.lookup.get_template("detail.html").render(id = _id, issues = issues, title = "Pull List Collection Detail")

    
    
    