# Author: Brett Vann <brett.vann@vannluminous.com>
# URL: http://github.com/roninbv/webslinger/
#
# This file is part of Web Slinger.
#
# Web Slinger is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Web Slinger is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Web Slinger.  If not, see <http://www.gnu.org/licenses/>.

import cherrypy
import json
import re
import webslinger
from cherrypy.lib.static import serve_file
from mako.lookup import TemplateLookup
from webslinger.Model.entities import IssueType


class CollectionController:
    def __init__(self, db):
        self.db = db
        self.lookup = TemplateLookup(directories=["webslinger/Views", "webslinger/Views/collection"])

    @cherrypy.expose
    def digital(self):
        return self.lookup.get_template("index.html").render(title="Digital Collection",
                                                             apiKey=webslinger.COMIC_VINE_API_KEY)

    @cherrypy.expose
    def printed(self):
        return self.lookup.get_template("index.html").render(title="Printed Collection")

    @cherrypy.expose
    def mappingdialog(self):
        return self.lookup.get_template("mappingdialog.html").render(apiKey=webslinger.COMIC_VINE_API_KEY)

    @cherrypy.expose
    def publishers(self):
        pubs = self.db.get_collection_publishers()
        return json.dumps(pubs)

    @cherrypy.expose
    @cherrypy.tools.json_in()
    def menu(self):
        data = cherrypy.request.json
        titles = self.db.get_collection_titles(data)
        menu = []

        for title in titles:
            if len(title[1]) > 18:
                truncated_title = title[1][:15] + "..."
            else:
                truncated_title = title[1]

            menu.append({"label": truncated_title + "  (" + str(title[2]) + ")", "id": title[0], "cv_id": title[3],
                         "title": title[1]})

        return json.dumps(menu)

    @cherrypy.expose
    def issues(self, series, page=1, issue_type=IssueType.NORMAL):
        col_issues = self.db.get_collection_issues(series, int(page), issue_type=issue_type)
        return json.dumps(col_issues)

    @cherrypy.expose
    def update_read(self, issue_id, is_read ):
        is_read = is_read == 'true'
        self.db.update_issue_read(issue_id, is_read)
        return json.dumps({'read': is_read})
    
    @cherrypy.expose
    def map_series(self, vm):

        try:
            title = json.loads(vm)
            self.db.map_series_to_pull_list(title)
            return json.dumps({"success": True})
        except:
            return json.dumps({"success": False})

    @cherrypy.expose
    def is_in_pull(self, Id):
        result = self.db.get_is_in_pull(Id)
        return json.dumps({"in_pull": result})


    @cherrypy.expose
    def thumb(self, Id):
        cherrypy.response.headers["Content-Type"] = "image/jpeg"
        return self.db.get_thumb(Id)[0]

    @cherrypy.expose
    def image(self, Id=None):
        cherrypy.response.headers["Content-Type"] = "image/jpeg"
        return self.db.get_image(Id)

    @cherrypy.expose
    def missing(self):
        result = self.db.get_missing_issues()
        return json.dumps({"count": len(result), "issues": result})

    @cherrypy.expose
    def download(self, filepath):
        return serve_file(filepath, "application/x-download", "attachment")
